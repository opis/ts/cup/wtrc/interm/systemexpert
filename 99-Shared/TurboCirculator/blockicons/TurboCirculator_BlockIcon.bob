<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-10-17 08:16:00 by joricklochet-->
<display version="2.0.0">
  <name>TurboCirculator</name>
  <width>132</width>
  <height>260</height>
  <widget type="label" version="2.0.0">
    <name>WID_AUTMANIcon</name>
    <text>A</text>
    <x>51</x>
    <y>24</y>
    <width>30</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="24.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="TextRule" prop_id="text" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>A</value>
        </exp>
        <exp bool_exp="pv1 == true">
          <value>M</value>
        </exp>
        <exp bool_exp="pv2 == true">
          <value>F</value>
        </exp>
        <pv_name>$(PLCName):V_001_OpMode_Auto</pv_name>
        <pv_name>$(PLCName):V_001_OpMode_Manual</pv_name>
        <pv_name>$(PLCName):V_001_OpMode_Forced</pv_name>
      </rule>
    </rules>
    <tooltip>Opmode indicator</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_AlarmIcon</name>
    <symbols>
      <symbol>../symbols/error@32.png</symbol>
    </symbols>
    <x>99</x>
    <y>31</y>
    <width>24</width>
    <height>24</height>
    <actions>
    </actions>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == true || pv1 == true">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == false &amp;&amp; pv1 == false">
          <value>false</value>
        </exp>
        <pv_name>$(WIDESSDeviceName):GroupAlarm</pv_name>
        <pv_name>$(WIDESSControllerName):GroupAlarm</pv_name>
      </rule>
    </rules>
    <tooltip>Alarm event occured!</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_Interlock</name>
    <symbols>
      <symbol>../symbols/interlock_overridden_disabled_cms@32.png</symbol>
    </symbols>
    <x>11</x>
    <y>31</y>
    <width>24</width>
    <height>24</height>
    <actions>
    </actions>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == false">
          <value>false</value>
        </exp>
        <pv_name>$(WIDESSDeviceName):StartInterlock</pv_name>
      </rule>
    </rules>
    <tooltip>Interlock event occured!</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_CenterIcon</name>
    <pv_name>$(WIDESSDeviceName):CompressorColor</pv_name>
    <symbols>
      <symbol>../symbols/Turbo_Circulator_NOT_CONTROLED@64.png</symbol>
      <symbol>../symbols/Turbo_Circulator_ERROR@64.png</symbol>
      <symbol>../symbols/Turbo_Circulator_WARNING@64.png</symbol>
      <symbol>../symbols/Turbo_Circulator_OK@64.png</symbol>
    </symbols>
    <x>34</x>
    <y>55</y>
    <width>64</width>
    <height>64</height>
    <rotation>90.0</rotation>
    <actions execute_as_one="true">
    </actions>
    <tooltip>Open faceplate</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_TitleLBL</name>
    <text>$(WIDDeviceName)</text>
    <width>132</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="24.0">
      </font>
    </font>
    <foreground_color>
      <color name="TEXT" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <background_color>
      <color red="0" green="0" blue="0" alpha="0">
      </color>
    </background_color>
    <horizontal_alignment>1</horizontal_alignment>
    <tooltip>Device name</tooltip>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>MET_PumpSpeed_11</name>
    <pv_name>$(WIDESSDeviceName):Circulator_SetPoint</pv_name>
    <y>158</y>
    <width>132</width>
    <height>33</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="24.0">
      </font>
    </font>
    <precision>0</precision>
    <rules>
      <rule name="Disconnected" prop_id="background_color" out_exp="false">
        <exp bool_exp="pvInt0 &gt; 0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pvInt0 == 0">
          <value>
            <color name="INVALID" red="149" green="110" blue="221">
            </color>
          </value>
        </exp>
        <pv_name>$(PLCName):PLCHashCorrectR</pv_name>
      </rule>
    </rules>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_width>1</border_width>
    <border_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>MET_PumpSpeed_10</name>
    <pv_name>$(WIDESSDeviceName):AMB_Rotor_Speed</pv_name>
    <y>124</y>
    <width>132</width>
    <height>33</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="24.0">
      </font>
    </font>
    <precision>1</precision>
    <rules>
      <rule name="Disconnected" prop_id="background_color" out_exp="false">
        <exp bool_exp="pvInt0 &gt; 0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pvInt0 == 0">
          <value>
            <color name="INVALID" red="149" green="110" blue="221">
            </color>
          </value>
        </exp>
        <pv_name>$(PLCName):PLCHashCorrectR</pv_name>
      </rule>
    </rules>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_width>1</border_width>
    <border_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_Value_PID_SP_3</name>
    <pv_name>$(WIDESSControllerName):MAN_SP</pv_name>
    <y>226</y>
    <width>82</width>
    <height>33</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="24.0">
      </font>
    </font>
    <precision>3</precision>
    <rules>
      <rule name="Disconnected" prop_id="background_color" out_exp="false">
        <exp bool_exp="pvInt0 &gt; 0">
          <value>
            <color name="Transparent" red="255" green="255" blue="255" alpha="0">
            </color>
          </value>
        </exp>
        <exp bool_exp="pvInt0 == 0">
          <value>
            <color name="INVALID" red="149" green="110" blue="221">
            </color>
          </value>
        </exp>
        <pv_name>$(PLCName):PLCHashCorrectR</pv_name>
      </rule>
      <rule name="Visible Rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>$(WIDESSControllerName):Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>PID Setpoint</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_width>1</border_width>
    <border_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_Value_PID_PV_1</name>
    <pv_name>$(WIDESSControllerName):ProcessValue</pv_name>
    <y>192</y>
    <width>82</width>
    <height>33</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="24.0">
      </font>
    </font>
    <precision>3</precision>
    <rules>
      <rule name="Disconnected" prop_id="background_color" out_exp="false">
        <exp bool_exp="pvInt0 &gt; 0">
          <value>
            <color name="Transparent" red="255" green="255" blue="255" alpha="0">
            </color>
          </value>
        </exp>
        <exp bool_exp="pvInt0 == 0">
          <value>
            <color name="INVALID" red="149" green="110" blue="221">
            </color>
          </value>
        </exp>
        <pv_name>$(PLCName):PLCHashCorrectR</pv_name>
      </rule>
      <rule name="Visible Rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>$(WIDESSControllerName):Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>PID Measured Feedback Value</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_width>1</border_width>
    <border_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_Value_PID_SP_4</name>
    <pv_name>$(WIDESSControllerName):ProcValueEGU</pv_name>
    <x>82</x>
    <y>192</y>
    <width>50</width>
    <height>33</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="24.0">
      </font>
    </font>
    <precision>0</precision>
    <rules>
      <rule name="Disconnected" prop_id="background_color" out_exp="false">
        <exp bool_exp="pvInt0 &gt; 0">
          <value>
            <color name="Transparent" red="255" green="255" blue="255" alpha="0">
            </color>
          </value>
        </exp>
        <exp bool_exp="pvInt0 == 0">
          <value>
            <color name="INVALID" red="149" green="110" blue="221">
            </color>
          </value>
        </exp>
        <pv_name>$(PLCName):PLCHashCorrectR</pv_name>
      </rule>
      <rule name="Visible Rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>$(WIDESSControllerName):Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>PID Setpoint</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_width>1</border_width>
    <border_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_Value_PID_SP_5</name>
    <pv_name>$(WIDESSControllerName):ProcValueEGU</pv_name>
    <x>82</x>
    <y>226</y>
    <width>50</width>
    <height>33</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="24.0">
      </font>
    </font>
    <precision>0</precision>
    <rules>
      <rule name="Disconnected" prop_id="background_color" out_exp="false">
        <exp bool_exp="pvInt0 &gt; 0">
          <value>
            <color name="Transparent" red="255" green="255" blue="255" alpha="0">
            </color>
          </value>
        </exp>
        <exp bool_exp="pvInt0 == 0">
          <value>
            <color name="INVALID" red="149" green="110" blue="221">
            </color>
          </value>
        </exp>
        <pv_name>$(PLCName):PLCHashCorrectR</pv_name>
      </rule>
      <rule name="Visible Rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>$(WIDESSControllerName):Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>PID Setpoint</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_width>1</border_width>
    <border_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </border_color>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>WID_OpenFaceplate</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>../faceplates/P_TurboCirculator_PID_Faceplate.bob</file>
        <macros>
          <ControllerName>$(WIDControllerName)</ControllerName>
          <ESSControllerName>$(WIDESSControllerName)</ESSControllerName>
          <DeviceName>$(WIDDeviceName)</DeviceName>
          <ESSDeviceName>$(WIDESSDeviceNameShort)</ESSDeviceName>
        </macros>
        <target>window</target>
      </action>
    </actions>
    <text></text>
    <x>1</x>
    <width>130</width>
    <height>259</height>
    <transparent>true</transparent>
    <tooltip>Open faceplate</tooltip>
  </widget>
</display>
