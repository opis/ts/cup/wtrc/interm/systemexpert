<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-04-11 10:46:13 by EpingerBalint-->
<display version="2.0.0">
  <name>1042_Fault</name>
  <macros>
    <PLCName>Tgt-MRCS:Ctrl-PLC-001</PLCName>
  </macros>
  <width>2550</width>
  <height>1220</height>
  <background_color>
    <color name="BACKGROUND" red="220" green="225" blue="221">
    </color>
  </background_color>
  <grid_color>
    <color name="TEXT-LIGHT" red="230" green="230" blue="230">
    </color>
  </grid_color>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <file>../../Header/WtrC_Header.bob</file>
    <width>2550</width>
    <height>85</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>X10</name>
    <macros>
      <StepName>FAULT</StepName>
      <WIDDev>FSM</WIDDev>
      <WIDDis>SC</WIDDis>
      <WIDIndex>010</WIDIndex>
      <WIDSecSub>Tgt-PWCR1042</WIDSecSub>
      <Faceplate>../../System_Specific_Components/1042/States/1042_FAULT.bob</Faceplate>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_BlockIcon_Standard_Compact.bob</file>
    <x>2040</x>
    <y>211</y>
    <width>500</width>
    <height>310</height>
    <resize>1</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BlkIconLabel</name>
    <text>State Block Icon</text>
    <x>2040</x>
    <y>170</y>
    <width>490</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_2</name>
    <actions>
      <action type="open_display">
        <file>1042_OFF.bob</file>
        <macros>
          <StepDeviceName>Tgt-PWCR1042:SC-FSM-001</StepDeviceName>
        </macros>
        <target>replace</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>To Off</text>
    <x>2390</x>
    <y>100</y>
    <width>160</width>
    <height>35</height>
    <tooltip>$(actions)</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <confirm_message>Open MAINTENANCE state view</confirm_message>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ParameterLabel</name>
    <text>State Parameters</text>
    <x>30</x>
    <y>170</y>
    <width>760</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter1</name>
    <macros>
      <Description>Residual Heat Removal Timer</Description>
      <EngUnit>min</EngUnit>
      <ParameterID>S1</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>210</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks</name>
    <text>State Specific Interlocks</text>
    <x>1430</x>
    <y>170</y>
    <width>580</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks_1</name>
    <text>State Entry Permissives</text>
    <x>820</x>
    <y>170</y>
    <width>580</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter2_1</name>
    <macros>
      <Description>1042-PICA-114/151 Main loop pressure control SP</Description>
      <EngUnit>barg</EngUnit>
      <ParameterID>S2</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>240</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter3_1</name>
    <macros>
      <Description>1042-FICA-150 Minimum pump flow control SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S3</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>270</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4_6</name>
    <macros>
      <Description>1042-HICA-124 Cooling water flow to Lower reflector SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S4</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>300</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4_7</name>
    <macros>
      <Description>1042-HICA-125 Cooling water flow to Upper reflector SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S5</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>330</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4_8</name>
    <macros>
      <Description>1042-HICA-102 De-ionized water flow control SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S6</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>360</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4_9</name>
    <macros>
      <Description>1042-TICA-109 Main loop temperature control SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S7</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>390</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4_10</name>
    <macros>
      <Description>1051-FICA-222 Cooling water through purification SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S8</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>420</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_22</name>
    <pv_name>${StepDeviceName}:IntlckDevPerm1</pv_name>
    <x>1430</x>
    <y>211</y>
    <width>280</width>
    <height>177</height>
    <numBits>6</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="LED-YELLOW-ON" red="255" green="235" blue="17">
      </color>
    </on_color>
    <labels>
      <text>1042-PICA-114</text>
      <text>1042-PICA-151</text>
      <text>1042-HICA-102</text>
      <text>1042-TICA-109</text>
      <text>1042-FICA-150</text>
      <text>Fill &amp; Refill Substate</text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
    </labels>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_23</name>
    <pv_name>${StepDeviceName}:IntlckDevPerm2</pv_name>
    <x>1730</x>
    <y>211</y>
    <width>280</width>
    <height>412</height>
    <numBits>14</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="LED-YELLOW-ON" red="255" green="235" blue="17">
      </color>
    </on_color>
    <labels>
      <text>1051-YSV-201</text>
      <text>1051-YSV-204</text>
      <text>1051-YSV-205</text>
      <text>1051-YSV-206</text>
      <text>1051-YSV-207</text>
      <text>1051-YSV-209</text>
      <text>1051-YSV-211</text>
      <text>1051-YSV-214</text>
      <text>1051-YSV-215</text>
      <text>1051-YSV-216</text>
      <text>1051-YSV-217</text>
      <text>1051-YSV-219</text>
      <text>1051-YSV-224</text>
      <text>1051-FICA-222</text>
      <text>Label 14</text>
      <text>Label 15</text>
    </labels>
  </widget>
</display>
