<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-04-30 09:14:05 by EpingerBalint-->
<display version="2.0.0">
  <name>1043_Running</name>
  <macros>
    <PLCName>Tgt-SPCS:Ctrl-PLC-001</PLCName>
  </macros>
  <width>2550</width>
  <height>1220</height>
  <background_color>
    <color name="BACKGROUND" red="220" green="225" blue="221">
    </color>
  </background_color>
  <grid_color>
    <color name="TEXT-LIGHT" red="230" green="230" blue="230">
    </color>
  </grid_color>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <file>../../Header/WtrC_Header.bob</file>
    <width>2550</width>
    <height>85</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>X4</name>
    <macros>
      <StepName>RUNNING</StepName>
      <WIDDev>FSM</WIDDev>
      <WIDDis>SC</WIDDis>
      <WIDIndex>004</WIDIndex>
      <WIDSecSub>Tgt-PWCM1043</WIDSecSub>
      <Faceplate>../../System_Specific_Components/1043/States/1043_RUNNING.bob</Faceplate>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_BlockIcon_Standard_Compact.bob</file>
    <x>2040</x>
    <y>211</y>
    <width>500</width>
    <height>310</height>
    <resize>1</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BlkIconLabel_1</name>
    <text>State Block Icon</text>
    <x>2040</x>
    <y>170</y>
    <width>490</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_1</name>
    <actions>
      <action type="open_display">
        <file>1043_STANDBY.bob</file>
        <macros>
          <StepDeviceName>Tgt-PWCS1043:SC-FSM-002</StepDeviceName>
        </macros>
        <target>replace</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>To StandBy</text>
    <x>30</x>
    <y>100</y>
    <width>160</width>
    <height>35</height>
    <tooltip>$(actions)</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <confirm_message>Open MAINTENANCE state view</confirm_message>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_2</name>
    <actions>
      <action type="open_display">
        <file>1043_FAULT.bob</file>
        <macros>
          <StepDeviceName>Tgt-PWCS1043:SC-FSM-010</StepDeviceName>
        </macros>
        <target>replace</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>To Fault</text>
    <x>2370</x>
    <y>100</y>
    <width>160</width>
    <height>35</height>
    <tooltip>$(actions)</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <confirm_message>Open MAINTENANCE state view</confirm_message>
  </widget>
  <widget type="label" version="2.0.0">
    <name>FlagLabel</name>
    <text>State Flag Status</text>
    <x>30</x>
    <y>710</y>
    <width>760</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ParameterLabel</name>
    <text>State Parameters</text>
    <x>30</x>
    <y>170</y>
    <width>760</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks</name>
    <text>State Specific Interlocks</text>
    <x>1430</x>
    <y>170</y>
    <width>580</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks_1</name>
    <text>State Entry Permissives</text>
    <x>820</x>
    <y>170</y>
    <width>580</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter1</name>
    <macros>
      <Description>Residual Heat Removal Timer</Description>
      <EngUnit>min</EngUnit>
      <ParameterID>S1</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>211</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter2</name>
    <macros>
      <Description>1043-PICA-114/151 Main loop pressure control SP</Description>
      <EngUnit>barg</EngUnit>
      <ParameterID>S2</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>241</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter3</name>
    <macros>
      <Description>1043-FICA-150 Minimum pump flow control SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S3</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>271</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4</name>
    <macros>
      <Description>1043-HICA-124 Cooling water flow to Shielding SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S4</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>301</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_16</name>
    <pv_name>${StepDeviceName}:MeasPerm1</pv_name>
    <x>820</x>
    <y>212</y>
    <width>130</width>
    <height>440</height>
    <numBits>16</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </on_color>
    <labels>
      <text>FIT-113 OK</text>
      <text>FIT-124 OK</text>
      <text>FIT-125 OK</text>
      <text>FIT-129 OK</text>
      <text>FIT-130 OK</text>
      <text>FIT-131 OK</text>
      <text>FIT-132 OK</text>
      <text>FIT-135 OK</text>
      <text>FIT-136 OK</text>
      <text>FIT-137 OK</text>
      <text>FIT-139 OK</text>
      <text>FIT-142 OK</text>
      <text>LIA-104 OK</text>
      <text>PIT-103 OK</text>
      <text>PIT-114 OK</text>
      <text>PIT-118 OK</text>
    </labels>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_19</name>
    <pv_name>${StepDeviceName}:MeasPerm2</pv_name>
    <x>970</x>
    <y>212</y>
    <width>130</width>
    <height>440</height>
    <numBits>16</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </on_color>
    <labels>
      <text>TI-105 OK</text>
      <text>TI-109 OK</text>
      <text>TI-140 OK</text>
      <text>TI-141 OK</text>
      <text>TI-142 OK</text>
      <text>TI-143 OK</text>
      <text>TI-144 OK</text>
      <text>TI-145 OK</text>
      <text>TI-146 OK</text>
      <text>TI-147 OK</text>
      <text>TI-148 OK</text>
      <text>TI-149 OK</text>
      <text>TI-150 OK</text>
      <text>TI-151 OK</text>
      <text>TI-152 OK</text>
      <text>TI-153 OK</text>
    </labels>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_21</name>
    <pv_name>${StepDeviceName}:MeasPerm3</pv_name>
    <x>1120</x>
    <y>212</y>
    <width>130</width>
    <height>440</height>
    <numBits>16</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </on_color>
    <labels>
      <text>TI-154 OK</text>
      <text>TI-155 OK</text>
      <text>TI-156 OK</text>
      <text>TI-157 OK</text>
      <text>TI-158 OK</text>
      <text>TI-159 OK</text>
      <text>TI-160 OK</text>
      <text>TI-161 OK</text>
      <text>TI-162 OK</text>
      <text>TI-163 OK</text>
      <text>TI-164 OK</text>
      <text>QIT-112 OK</text>
      <text>W-002 OK</text>
      <text>1043 Pump(s) OK</text>
      <text>FCV-114 OK</text>
      <text>Control Valves OK</text>
    </labels>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4_1</name>
    <macros>
      <Description>1043-HICA-125 Cooling water flow to Shielding SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S5</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>331</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4_2</name>
    <macros>
      <Description>1043-HICA-129 CW to TMP cooling block SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S6</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>361</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4_3</name>
    <macros>
      <Description>1043-HICA-130 CW flow to PBIP SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S7</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>391</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4_4</name>
    <macros>
      <Description>1043-HICA-102 De-ionized water flow control SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S8</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>421</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4_5</name>
    <macros>
      <Description>1043-TICA-109 Main loop temperature control SP</Description>
      <EngUnit>degC</EngUnit>
      <ParameterID>S9</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>451</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4_6</name>
    <macros>
      <Description>1052-FICA-122 Cooling water through purification SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S10</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>481</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_22</name>
    <pv_name>${StepDeviceName}:MeasPerm4</pv_name>
    <x>1270</x>
    <y>212</y>
    <width>130</width>
    <height>330</height>
    <numBits>12</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </on_color>
    <labels>
      <text>PSZ-101 OK</text>
      <text>PSZ-102 OK</text>
      <text>PSZ-103 OK</text>
      <text>PSZ-104 OK</text>
      <text>PSZ-105 OK</text>
      <text>1052-TI-353 OK</text>
      <text>1052-PI-352 OK</text>
      <text>1052-QI-352 OK</text>
      <text>1052-FIT-322 OK</text>
      <text>1052-PDS-251 OK</text>
      <text>1052-FCV-322 OK</text>
      <text>Purification ON</text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
    </labels>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter11</name>
    <macros>
      <Description>1043-HICA-129 Cooling water flow to Lower moderators SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S11</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>511</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter12</name>
    <macros>
      <Description>1043-HICA-130 Cooling water flow to Lower moderators SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S12</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>541</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4_10</name>
    <macros>
      <Description>1043-TICA-109 Main loop temperature control SP</Description>
      <EngUnit>degC</EngUnit>
      <ParameterID>S13</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>571</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter15</name>
    <macros>
      <Description>1052-FICA-122 Cooling water through purification SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S14</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>601</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter16</name>
    <macros>
      <Description>1052-FICA-122 Cooling water through purification SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S15</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>631</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_23</name>
    <pv_name>${StepDeviceName}:IntlckDevPerm1</pv_name>
    <x>1430</x>
    <y>212</y>
    <width>280</width>
    <height>177</height>
    <numBits>6</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="LED-YELLOW-ON" red="255" green="235" blue="17">
      </color>
    </on_color>
    <labels>
      <text>1043-PICA-114</text>
      <text>1043-PICA-151</text>
      <text>1043-HICA-102</text>
      <text>1043-TICA-109</text>
      <text>1043-FICA-150</text>
      <text>Fill &amp; Refill Substate</text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
    </labels>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_24</name>
    <pv_name>${StepDeviceName}:IntlckDevPerm2</pv_name>
    <x>1730</x>
    <y>212</y>
    <width>280</width>
    <height>412</height>
    <numBits>14</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="LED-YELLOW-ON" red="255" green="235" blue="17">
      </color>
    </on_color>
    <labels>
      <text>1052-YSV-301</text>
      <text>1052-YSV-304</text>
      <text>1052-YSV-305</text>
      <text>1052-YSV-306</text>
      <text>1052-YSV-307</text>
      <text>1052-YSV-309</text>
      <text>1052-YSV-311</text>
      <text>1052-YSV-314</text>
      <text>1052-YSV-315</text>
      <text>1052-YSV-316</text>
      <text>1052-YSV-317</text>
      <text>1052-YSV-219</text>
      <text>1052-YSV-324</text>
      <text>1052-FICA-322</text>
      <text>Label 14</text>
      <text>Label 15</text>
    </labels>
  </widget>
</display>
