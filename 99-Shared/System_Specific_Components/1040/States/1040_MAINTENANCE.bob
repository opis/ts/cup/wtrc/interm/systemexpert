<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-07-22 11:33:08 by EpingerBalint-->
<display version="2.0.0">
  <name>RGT 1040 - MAINTENANCE</name>
  <macros>
    <PLCName>Tgt-RGT1040:Ctrl-PLC-001</PLCName>
    <StateDeviceName>Tgt-RGT1040:SC-FSM-006</StateDeviceName>
  </macros>
  <width>2550</width>
  <height>1220</height>
  <background_color>
    <color name="BACKGROUND" red="220" green="225" blue="221">
    </color>
  </background_color>
  <grid_color>
    <color name="TEXT-LIGHT" red="230" green="230" blue="230">
    </color>
  </grid_color>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <macros>
      <Title>Radiolysis Gas Treatment System - 1040 - Maintenance State</Title>
    </macros>
    <file>../../Header/WtrC_Header.bob</file>
    <width>2550</width>
    <height>85</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>X2_2</name>
    <macros>
      <Faceplate>../../System_Specific_Components/1040/States/1040_MAINTENANCE.bob</Faceplate>
      <StepName>MAINTENANCE</StepName>
      <WIDDev>FSM</WIDDev>
      <WIDDis>SC</WIDDis>
      <WIDIndex>006</WIDIndex>
      <WIDSecSub>Tgt-RGT1040</WIDSecSub>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_BlockIcon_Standard_Compact.bob</file>
    <x>2040</x>
    <y>210</y>
    <width>500</width>
    <height>350</height>
    <resize>1</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BlkIconLabel_3</name>
    <text>State status</text>
    <x>2040</x>
    <y>170</y>
    <width>490</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BlkIconLabel_4</name>
    <text>Substates status</text>
    <x>2040</x>
    <y>680</y>
    <width>480</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button</name>
    <actions>
      <action type="open_display">
        <file>1040_OFF.bob</file>
        <macros>
          <StepDeviceName>Tgt-RGT1040:SC-FSM-001</StepDeviceName>
        </macros>
        <target>replace</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>To OFF</text>
    <x>2370</x>
    <y>100</y>
    <width>160</width>
    <height>35</height>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>X1_8</name>
    <macros>
      <Faceplate>../../System_Specific_Components/1040/States/1040_MAINTENANCE.bob</Faceplate>
      <StateName>FLUSHING</StateName>
      <WIDDev>FSM</WIDDev>
      <WIDDis>SC</WIDDis>
      <WIDIndex>061</WIDIndex>
      <WIDSecSub>Tgt-RGT1040</WIDSecSub>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_BlockIcon_SubState_Compact.bob</file>
    <x>2055</x>
    <y>730</y>
    <width>450</width>
    <height>220</height>
    <resize>1</resize>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_24</name>
    <pv_name>${StepDeviceName}:IntlckDevPerm1</pv_name>
    <x>1500</x>
    <y>210</y>
    <width>380</width>
    <height>350</height>
    <numBits>11</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="Attention" red="252" green="242" blue="17">
      </color>
    </on_color>
    <labels>
      <text>AE - 190</text>
      <text>W - 110</text>
      <text>P - 205</text>
      <text>P - 255</text>
      <text>YSV - 301</text>
      <text>YSV - 208</text>
      <text>YSV - 209</text>
      <text>YSV - 210</text>
      <text>YSV - 405</text>
      <text>YSV - 406</text>
      <text>YSV - 703</text>
    </labels>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>PermissivesWord1_3</name>
    <pv_name>${StepDeviceName}:MeasPerm1</pv_name>
    <x>950</x>
    <y>210</y>
    <width>180</width>
    <height>470</height>
    <numBits>16</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <labels>
      <text>FT - 105 OK</text>
      <text>FT -305 OK</text>
      <text>TT - 118 OK</text>
      <text>TT - 117 OK</text>
      <text>LT - 131 OK</text>
      <text>TT - 116 OK</text>
      <text>AI - 193 OK</text>
      <text>PT - 137 OK</text>
      <text>PT - 108 OK</text>
      <text>TT - 111 OK</text>
      <text>TT - 112 OK</text>
      <text>FT - 403 OK</text>
      <text>AI - 191 OK</text>
      <text>AI - 192 OK</text>
      <text>TT - 121 OK</text>
      <text>TT - 122 OK</text>
    </labels>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>PermissivesWord1_4</name>
    <pv_name>${StepDeviceName}:MeasPerm2</pv_name>
    <x>1150</x>
    <y>210</y>
    <width>180</width>
    <height>205</height>
    <numBits>7</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <labels>
      <text>AI - 194 OK</text>
      <text>II - 195 OK</text>
      <text>JI - 196 OK</text>
      <text>FT - 701 OK</text>
      <text>SC - 103 OK</text>
      <text>SC - 153 OK</text>
      <text>Timer OK</text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
    </labels>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ParameterLabel</name>
    <text>State Parameters</text>
    <x>30</x>
    <y>170</y>
    <width>760</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks</name>
    <text>State Specific Interlocks</text>
    <x>1500</x>
    <y>170</y>
    <width>380</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks_1</name>
    <text>State Entry Permissives</text>
    <x>950</x>
    <y>170</y>
    <width>380</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>FlagLabel</name>
    <text>State Flag Status</text>
    <x>29</x>
    <y>680</y>
    <width>760</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter1_2</name>
    <macros>
      <Description>Pressure equalize timer</Description>
      <EngUnit>min</EngUnit>
      <ParameterID>S1</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>212</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter2_2</name>
    <macros>
      <Description>Speed ratio between Blowers V-102:V-152</Description>
      <EngUnit>%</EngUnit>
      <ParameterID>S2</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>245</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter3_2</name>
    <macros>
      <Description>FICA-105/155 Gas loop flow control SP</Description>
      <EngUnit>m3/h</EngUnit>
      <ParameterID>S3</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>278</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4_1</name>
    <macros>
      <Description>LICA-131/151 Condensate tank level control  SP</Description>
      <EngUnit>%</EngUnit>
      <ParameterID>S4</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>311</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter5</name>
    <macros>
      <Description>Flushing timer</Description>
      <EngUnit>min</EngUnit>
      <ParameterID>S5</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>344</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
</display>
