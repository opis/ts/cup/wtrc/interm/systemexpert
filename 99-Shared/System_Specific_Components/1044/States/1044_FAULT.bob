<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-05-14 13:29:25 by EpingerBalint-->
<display version="2.0.0">
  <name>Fault</name>
  <macros>
    <PLCName>Tgt-MRCS:Ctrl-PLC-001</PLCName>
  </macros>
  <width>2557</width>
  <height>1285</height>
  <background_color>
    <color name="BACKGROUND" red="220" green="225" blue="221">
    </color>
  </background_color>
  <grid_color>
    <color name="TEXT-LIGHT" red="230" green="230" blue="230">
    </color>
  </grid_color>
  <widget type="label" version="2.0.0">
    <name>FlagLabel</name>
    <text>State Flag Status</text>
    <x>30</x>
    <y>680</y>
    <width>760</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ParameterLabel</name>
    <text>State Parameters</text>
    <x>30</x>
    <y>170</y>
    <width>760</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <macros>
      <Title>Intermediate Water Cooling 15 °C - 1044 - Fault State</Title>
    </macros>
    <file>../../Header/WtrC_Header.bob</file>
    <width>2550</width>
    <height>85</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>10</name>
    <macros>
      <StepName>FAULT</StepName>
      <WIDDev>FSM</WIDDev>
      <WIDDis>SC</WIDDis>
      <WIDIndex>010</WIDIndex>
      <WIDSecSub>Tgt-IWCL1044</WIDSecSub>
      <Faceplate>../../System_Specific_Components/1044/States/1044_FAULT.bob</Faceplate>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_BlockIcon_Standard_Compact.bob</file>
    <x>2040</x>
    <y>211</y>
    <width>500</width>
    <height>310</height>
    <resize>1</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BlkIconLabel</name>
    <text>State Block Icon</text>
    <x>2040</x>
    <y>170</y>
    <width>490</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4</name>
    <macros>
      <Description>1044-TICA-107 (Main loop temperature control) SP</Description>
      <EngUnit>degC</EngUnit>
      <ParameterID>S4</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>302</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter3</name>
    <macros>
      <Description>1044-PICA-104 (Main loop pressure control) SP</Description>
      <EngUnit>barg</EngUnit>
      <ParameterID>S2</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>241</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4</name>
    <macros>
      <Description>1044-FICA-150 (Main loop minimum flow control) SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S3</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>272</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter1</name>
    <macros>
      <Description>Residual Heat Removal Timer</Description>
      <EngUnit>min</EngUnit>
      <ParameterID>S1</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>210</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks</name>
    <text>State Specific Interlocks</text>
    <x>1500</x>
    <y>170</y>
    <width>380</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks_1</name>
    <text>State Entry Permissives</text>
    <x>950</x>
    <y>170</y>
    <width>380</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_2</name>
    <actions>
      <action type="open_display">
        <file>1044_OFF.bob</file>
        <macros>
          <StepDeviceName>Tgt-IWCL1044:SC-FSM-001</StepDeviceName>
        </macros>
        <target>replace</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>To Off</text>
    <x>2390</x>
    <y>100</y>
    <width>160</width>
    <height>35</height>
    <tooltip>$(actions)</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <confirm_message>Open MAINTENANCE state view</confirm_message>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_20</name>
    <pv_name>${StepDeviceName}:IntlckDevPerm1</pv_name>
    <x>1500</x>
    <y>211</y>
    <width>380</width>
    <height>150</height>
    <numBits>5</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="LED-YELLOW-ON" red="255" green="235" blue="17">
      </color>
    </on_color>
    <labels>
      <text>YSV-101 Deionized water inlet on-off valve</text>
      <text>Fill &amp; Refill Substate</text>
      <text>PICA-104 Main loop pressure control</text>
      <text>TICA-107 Main loop temperature control</text>
      <text>FICA-150 Main loop minimum flow control</text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
    </labels>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter2_1</name>
    <macros>
      <Description>1042-TICA-109 (1042 Main loop temperature control) SP</Description>
      <EngUnit>degC</EngUnit>
      <ParameterID>S6</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>362</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4_2</name>
    <macros>
      <Description>1041-TICA-109 (1041 Main loop temperature control) SP</Description>
      <EngUnit>degC</EngUnit>
      <ParameterID>S5</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>332</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
</display>
