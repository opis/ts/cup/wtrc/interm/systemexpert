<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-05-29 11:15:35 by EpingerBalint-->
<display version="2.0.0">
  <name>RGTCS Diagnostics</name>
  <macros>
    <PLCName>Tgt-RGT1040:Ctrl-PLC-001</PLCName>
  </macros>
  <width>1810</width>
  <height>1060</height>
  <background_color>
    <color name="BACKGROUND" red="220" green="225" blue="221">
    </color>
  </background_color>
  <widget type="rectangle" version="2.0.0">
    <name>group.CabinetUH05.border_1</name>
    <x>1080</x>
    <width>710</width>
    <height>420</height>
    <line_width>2</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="group" version="3.0.0">
    <name>PLC</name>
    <y>430</y>
    <width>1790</width>
    <height>300</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>group.PLC.border</name>
      <width>1790</width>
      <height>300</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="picture" version="2.0.0">
      <name>PLCRack</name>
      <file>RGTCS_MainRack.png</file>
      <x>35</x>
      <y>20</y>
      <width>827</width>
      <height>270</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_PLC</name>
      <text>PLC</text>
      <width>30</width>
      <height>300</height>
      <font>
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <foreground_color>
        <color name="TEXT" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <rotation_step>1</rotation_step>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_PLC_CM_KF02_NOK_2</name>
      <pv_name>${PLCName}:PLC_CM_KF02_NOK</pv_name>
      <x>206</x>
      <y>143</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="On" red="70" green="255" blue="70">
        </color>
      </off_color>
      <on_color>
        <color name="MAJOR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_PLC_DI_KF03_NOK_2</name>
      <pv_name>${PLCName}:PLC_DI_KF03_NOK</pv_name>
      <x>284</x>
      <y>143</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="On" red="70" green="255" blue="70">
        </color>
      </off_color>
      <on_color>
        <color name="MAJOR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_PLC_DI_KF04_NOK_2</name>
      <pv_name>${PLCName}:PLC_DI_KF04_NOK</pv_name>
      <x>358</x>
      <y>143</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="On" red="70" green="255" blue="70">
        </color>
      </off_color>
      <on_color>
        <color name="MAJOR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_CM_KF02_2</name>
      <text>CM = KF02</text>
      <x>206</x>
      <y>52</y>
      <width>26</width>
      <height>85</height>
      <font>
        <font family="Source Sans Pro" style="BOLD_ITALIC" size="16.0">
        </font>
      </font>
      <rotation_step>1</rotation_step>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_DI_KF03_4</name>
      <text>DI = KF03</text>
      <x>285</x>
      <y>52</y>
      <width>26</width>
      <height>85</height>
      <font>
        <font family="Source Sans Pro" style="BOLD_ITALIC" size="16.0">
        </font>
      </font>
      <rotation_step>1</rotation_step>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_DI_KF04_2</name>
      <text>DI = KF04</text>
      <x>359</x>
      <y>52</y>
      <width>26</width>
      <height>85</height>
      <font>
        <font family="Source Sans Pro" style="BOLD_ITALIC" size="16.0">
        </font>
      </font>
      <rotation_step>1</rotation_step>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_PLC_DO_KF05_NOK_2</name>
      <pv_name>${PLCName}:PLC_DI_KF05_NOK</pv_name>
      <x>435</x>
      <y>143</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="On" red="70" green="255" blue="70">
        </color>
      </off_color>
      <on_color>
        <color name="MAJOR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_PLC_DO_KF06_NOK</name>
      <pv_name>${PLCName}:PLC_DO_KF06_NOK</pv_name>
      <x>510</x>
      <y>143</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="On" red="70" green="255" blue="70">
        </color>
      </off_color>
      <on_color>
        <color name="MAJOR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_DO_KF05_11</name>
      <text>DI = KF05</text>
      <x>435</x>
      <y>52</y>
      <width>26</width>
      <height>85</height>
      <font>
        <font family="Source Sans Pro" style="BOLD_ITALIC" size="16.0">
        </font>
      </font>
      <rotation_step>1</rotation_step>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_DO_KF06_10</name>
      <text>DO = KF06</text>
      <x>511</x>
      <y>52</y>
      <width>26</width>
      <height>85</height>
      <font>
        <font family="Source Sans Pro" style="BOLD_ITALIC" size="16.0">
        </font>
      </font>
      <rotation_step>1</rotation_step>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_PLC_DO_KF07_NOK_2</name>
      <pv_name>${PLCName}:PLC_DO_KF07_NOK</pv_name>
      <x>584</x>
      <y>143</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="On" red="70" green="255" blue="70">
        </color>
      </off_color>
      <on_color>
        <color name="MAJOR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_PLC_AI_KF08_NOK</name>
      <pv_name>${PLCName}:PLC_AI_KF08_NOK</pv_name>
      <x>659</x>
      <y>143</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="On" red="70" green="255" blue="70">
        </color>
      </off_color>
      <on_color>
        <color name="MAJOR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_DO_KF05_12</name>
      <text>DO = KF07</text>
      <x>584</x>
      <y>52</y>
      <width>26</width>
      <height>85</height>
      <font>
        <font family="Source Sans Pro" style="BOLD_ITALIC" size="16.0">
        </font>
      </font>
      <rotation_step>1</rotation_step>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_AI_KF06_11</name>
      <text>AI = KF08</text>
      <x>659</x>
      <y>52</y>
      <width>26</width>
      <height>85</height>
      <font>
        <font family="Source Sans Pro" style="BOLD_ITALIC" size="16.0">
        </font>
      </font>
      <rotation_step>1</rotation_step>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_PLC_AI_KF09_NOK_2</name>
      <pv_name>${PLCName}:PLC_AI_KF09_NOK</pv_name>
      <x>735</x>
      <y>143</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="On" red="70" green="255" blue="70">
        </color>
      </off_color>
      <on_color>
        <color name="MAJOR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_AI_KF05_13</name>
      <text>AI = KF09</text>
      <x>735</x>
      <y>52</y>
      <width>26</width>
      <height>85</height>
      <font>
        <font family="Source Sans Pro" style="BOLD_ITALIC" size="16.0">
        </font>
      </font>
      <rotation_step>1</rotation_step>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_PLC_AI_KF10_NOK_2</name>
      <pv_name>${PLCName}:PLC_AI_KF10_NOK</pv_name>
      <x>811</x>
      <y>143</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="On" red="70" green="255" blue="70">
        </color>
      </off_color>
      <on_color>
        <color name="MAJOR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_DO_KF05_14</name>
      <text>AI = KF10</text>
      <x>811</x>
      <y>52</y>
      <width>26</width>
      <height>85</height>
      <font>
        <font family="Source Sans Pro" style="BOLD_ITALIC" size="16.0">
        </font>
      </font>
      <rotation_step>1</rotation_step>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_PLC_CPU_KF01_NOK_2</name>
      <pv_name>${PLCName}:PLC_CPU_KF01_NOK</pv_name>
      <x>97</x>
      <y>143</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="On" red="70" green="255" blue="70">
        </color>
      </off_color>
      <on_color>
        <color name="MAJOR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_CM_KF01_2</name>
      <text>CPU = KF01</text>
      <x>97</x>
      <y>52</y>
      <width>26</width>
      <height>85</height>
      <font>
        <font family="Source Sans Pro" style="BOLD_ITALIC" size="16.0">
        </font>
      </font>
      <rotation_step>1</rotation_step>
    </widget>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>group.RIO.border_3</name>
    <width>350</width>
    <height>420</height>
    <line_width>2</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>OnlineConnection</name>
    <file>../../../99-Shared/Diagnostics/Connection.bob</file>
    <x>10</x>
    <y>30</y>
    <width>330</width>
    <height>380</height>
    <resize>1</resize>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Cabinet_UH02</name>
    <x>360</x>
    <width>710</width>
    <height>420</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>group.CabinetUH05.border</name>
      <width>710</width>
      <height>420</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="rectangle" version="2.0.0">
      <name>group.CabinetUH05.background</name>
      <x>10</x>
      <y>30</y>
      <width>340</width>
      <height>380</height>
      <line_width>1</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="BACKGROUND" red="220" green="225" blue="221">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_CabinetSignals_UH08</name>
      <text>CABINET SIGNALS - UH08</text>
      <x>10</x>
      <width>700</width>
      <height>30</height>
      <font>
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <foreground_color>
        <color name="TEXT" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_CabinetPower_UH02</name>
      <pv_name>${PLCName}:FB_UH02_Power</pv_name>
      <x>32</x>
      <y>50</y>
      <width>25</width>
      <height>25</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LB_CabinetPower_UH05</name>
      <text>Cabinet Power</text>
      <x>77</x>
      <y>50</y>
      <width>150</width>
      <height>25</height>
      <background_color>
        <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
        </color>
      </background_color>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_CabinetFuses_UH02</name>
      <pv_name>${PLCName}:FB_UH02_Fuses</pv_name>
      <x>32</x>
      <y>95</y>
      <width>25</width>
      <height>25</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LB_CabinetFuses_UH05</name>
      <text>Cabinet Fuses</text>
      <x>77</x>
      <y>95</y>
      <width>150</width>
      <height>25</height>
      <background_color>
        <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
        </color>
      </background_color>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_CabinetDoor_UH02</name>
      <pv_name>${PLCName}:FB_UH02_CabDoor</pv_name>
      <x>32</x>
      <y>140</y>
      <width>25</width>
      <height>25</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LB_CabinetDoor_UH05</name>
      <text>Cabinet Door Closed</text>
      <x>77</x>
      <y>140</y>
      <width>150</width>
      <height>25</height>
      <background_color>
        <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
        </color>
      </background_color>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_TA01_UH02</name>
      <pv_name>${PLCName}:FB_UH02_TA01</pv_name>
      <x>32</x>
      <y>185</y>
      <width>25</width>
      <height>25</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LB_TA01_UH05</name>
      <text>-TA01 (24VDC PSU)</text>
      <x>77</x>
      <y>185</y>
      <width>150</width>
      <height>25</height>
      <background_color>
        <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
        </color>
      </background_color>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_TA02_UH02</name>
      <pv_name>${PLCName}:FB_UH02_TA02</pv_name>
      <x>32</x>
      <y>230</y>
      <width>25</width>
      <height>25</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LB_TA02_UH05</name>
      <text>-TA02 (24VDC PSU)</text>
      <x>77</x>
      <y>230</y>
      <width>150</width>
      <height>25</height>
      <background_color>
        <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
        </color>
      </background_color>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_FC101_UH02</name>
      <pv_name>${PLCName}:FB_UH02_FC101</pv_name>
      <x>32</x>
      <y>275</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="On" red="70" green="255" blue="70">
        </color>
      </off_color>
      <on_color>
        <color name="MAJOR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LB_FC101_UH05</name>
      <text>-FC101 (24VDC FUSE)</text>
      <x>77</x>
      <y>275</y>
      <width>150</width>
      <height>25</height>
      <background_color>
        <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
        </color>
      </background_color>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_FC102_UH02</name>
      <pv_name>${PLCName}:FB_UH02_FC102</pv_name>
      <x>32</x>
      <y>320</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="On" red="70" green="255" blue="70">
        </color>
      </off_color>
      <on_color>
        <color name="MAJOR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LB_FC102_UH05</name>
      <text>-FC102 (24VDC FUSE)</text>
      <x>77</x>
      <y>320</y>
      <width>150</width>
      <height>25</height>
      <background_color>
        <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
        </color>
      </background_color>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>BTN_RST_FC101_UH02</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <pv_name>${PLCName}:Cmd_ResetUH02FC101</pv_name>
      <text>RESET</text>
      <x>240</x>
      <y>271</y>
      <width>80</width>
      <height>33</height>
      <rules>
        <rule name="Visibility Rule" prop_id="visible" out_exp="false">
          <exp bool_exp="pv0 == 1">
            <value>true</value>
          </exp>
          <exp bool_exp="pv0 == 0">
            <value>false</value>
          </exp>
          <pv_name>${PLCName}:FB_UH02_FC101</pv_name>
        </rule>
      </rules>
      <tooltip>Reset Fuse</tooltip>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>BTN_RST_FC102_UH02</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <pv_name>${PLCName}:Cmd_ResetUH02FC102</pv_name>
      <text>RESET</text>
      <x>240</x>
      <y>315</y>
      <width>80</width>
      <height>33</height>
      <rules>
        <rule name="Visibility Rule" prop_id="visible" out_exp="false">
          <exp bool_exp="pv0 == 1">
            <value>true</value>
          </exp>
          <exp bool_exp="pv0 == 0">
            <value>false</value>
          </exp>
          <pv_name>${PLCName}:FB_UH02_FC102</pv_name>
        </rule>
      </rules>
      <tooltip>Reset Fuse</tooltip>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="rectangle" version="2.0.0">
      <name>group.CabinetUH05.background_1</name>
      <x>360</x>
      <y>30</y>
      <width>340</width>
      <height>380</height>
      <line_width>1</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="BACKGROUND" red="220" green="225" blue="221">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_FC102_UH02_1</name>
      <pv_name>${PLCName}:FB_UH02_FC103</pv_name>
      <x>32</x>
      <y>365</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="On" red="70" green="255" blue="70">
        </color>
      </off_color>
      <on_color>
        <color name="MAJOR" red="252" green="13" blue="27">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LB_FC102_UH05_1</name>
      <text>-FC103 (24VDC FUSE)</text>
      <x>77</x>
      <y>365</y>
      <width>150</width>
      <height>25</height>
      <background_color>
        <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
        </color>
      </background_color>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>BTN_RST_FC102_UH02_1</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <pv_name>${PLCName}:Cmd_ResetUH02FC103</pv_name>
      <text>RESET</text>
      <x>240</x>
      <y>360</y>
      <width>80</width>
      <height>33</height>
      <rules>
        <rule name="Visibility Rule" prop_id="visible" out_exp="false">
          <exp bool_exp="pv0 == 1">
            <value>true</value>
          </exp>
          <exp bool_exp="pv0 == 0">
            <value>false</value>
          </exp>
          <pv_name>${PLCName}:FB_UH02_FC103</pv_name>
        </rule>
      </rules>
      <tooltip>Reset Fuse</tooltip>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Cabinet_UH01</name>
    <x>1080</x>
    <width>710</width>
    <height>420</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>group.CabinetUH05.background_2</name>
      <x>10</x>
      <y>30</y>
      <width>340</width>
      <height>380</height>
      <line_width>1</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="BACKGROUND" red="220" green="225" blue="221">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED_CabinetPower_UH01</name>
      <pv_name>${PLCName}:FB_UH01_Power</pv_name>
      <x>32</x>
      <y>50</y>
      <width>25</width>
      <height>25</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LB_CabinetPower_UH05_1</name>
      <text>Cabinet Fault</text>
      <x>77</x>
      <y>50</y>
      <width>150</width>
      <height>25</height>
      <background_color>
        <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
        </color>
      </background_color>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_CabinetSignals_K01</name>
      <text>CABINET SIGNALS (RAMAN) - K01</text>
      <width>690</width>
      <height>30</height>
      <font>
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <foreground_color>
        <color name="TEXT" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="rectangle" version="2.0.0">
      <name>group.CabinetUH05.background_3</name>
      <x>360</x>
      <y>30</y>
      <width>340</width>
      <height>380</height>
      <line_width>1</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="BACKGROUND" red="220" green="225" blue="221">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_CabinetSignals_UH05_3</name>
    <text>PLC Communication</text>
    <x>15</x>
    <width>325</width>
    <height>30</height>
    <font>
      <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
      </font>
    </font>
    <foreground_color>
      <color name="TEXT" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>group.PLC.border_1</name>
    <y>740</y>
    <width>1790</width>
    <height>300</height>
    <line_width>2</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_RIORack</name>
    <text>REMOTE IO</text>
    <y>740</y>
    <width>30</width>
    <height>300</height>
    <font>
      <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
      </font>
    </font>
    <foreground_color>
      <color name="TEXT" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <rotation_step>1</rotation_step>
  </widget>
</display>
