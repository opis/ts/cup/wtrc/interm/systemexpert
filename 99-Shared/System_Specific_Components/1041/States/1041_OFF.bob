<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-04-10 10:40:13 by EpingerBalint-->
<display version="2.0.0">
  <name>PWCM1041 - OFF</name>
  <macros>
    <PLCName>Tgt-MRCS:Ctrl-PLC-001</PLCName>
  </macros>
  <width>2550</width>
  <height>1220</height>
  <background_color>
    <color name="BACKGROUND" red="220" green="225" blue="221">
    </color>
  </background_color>
  <grid_color>
    <color name="TEXT-LIGHT" red="230" green="230" blue="230">
    </color>
  </grid_color>
  <widget type="label" version="2.0.0">
    <name>FlagLabel</name>
    <text>State Flag Status</text>
    <x>30</x>
    <y>680</y>
    <width>760</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ParameterLabel</name>
    <text>State Parameters</text>
    <x>30</x>
    <y>170</y>
    <width>760</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <macros>
      <Title>Primary Water Cooling Moderators - 1041 - OFF State</Title>
    </macros>
    <file>../../Header/WtrC_Header.bob</file>
    <width>2550</width>
    <height>85</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>X1</name>
    <macros>
      <StepName>OFF</StepName>
      <WIDDev>FSM</WIDDev>
      <WIDDis>SC</WIDDis>
      <WIDIndex>001</WIDIndex>
      <WIDSecSub>Tgt-PWCM1041</WIDSecSub>
      <Faceplate>../../System_Specific_Components/1041/States/1041_OFF.bob</Faceplate>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_BlockIcon_Standard_Compact.bob</file>
    <x>2040</x>
    <y>211</y>
    <width>500</width>
    <height>310</height>
    <resize>1</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BlkIconLabel_1</name>
    <text>State Block Icon</text>
    <x>2040</x>
    <y>170</y>
    <width>490</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button</name>
    <actions>
      <action type="open_display">
        <file>1041_STANDBY.bob</file>
        <macros>
          <StepDeviceName>Tgt-PWCM1041:SC-FSM-002</StepDeviceName>
        </macros>
        <target>replace</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>To Standby</text>
    <x>2390</x>
    <y>100</y>
    <width>160</width>
    <height>35</height>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_1</name>
    <actions>
      <action type="open_display">
        <file>1041_MAINTENANCE.bob</file>
        <macros>
          <StepDeviceName>Tgt-PWCM1041:SC-FSM-006</StepDeviceName>
        </macros>
        <target>replace</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>To MAINTENANCE</text>
    <x>10</x>
    <y>100</y>
    <width>160</width>
    <height>35</height>
    <tooltip>$(actions)</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <confirm_message>Open MAINTENANCE state view</confirm_message>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks</name>
    <text>State Specific Interlocks</text>
    <x>1430</x>
    <y>170</y>
    <width>580</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks_1</name>
    <text>State Entry Permissives</text>
    <x>820</x>
    <y>170</y>
    <width>580</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_20</name>
    <pv_name>${StepDeviceName}:IntlckDevPerm1</pv_name>
    <x>1430</x>
    <y>211</y>
    <width>180</width>
    <height>177</height>
    <numBits>6</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="LED-YELLOW-ON" red="255" green="235" blue="17">
      </color>
    </on_color>
    <labels>
      <text>1041-PICA-114</text>
      <text>1041-PICA-151</text>
      <text>1041-HICA-102</text>
      <text>1041-TICA-109</text>
      <text>1041-FICA-150</text>
      <text>Fill &amp; Refill Substate</text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
    </labels>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_23</name>
    <pv_name>${StepDeviceName}:IntlckDevPerm2</pv_name>
    <x>1630</x>
    <y>211</y>
    <width>180</width>
    <height>382</height>
    <numBits>13</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="LED-YELLOW-ON" red="255" green="235" blue="17">
      </color>
    </on_color>
    <labels>
      <text>1050-YSV-101</text>
      <text>1050-YSV-104</text>
      <text>1050-YSV-105</text>
      <text>1050-YSV-106</text>
      <text>1050-YSV-107</text>
      <text>1050-YSV-109</text>
      <text>1050-YSV-111</text>
      <text>1050-YSV-114</text>
      <text>1050-YSV-115</text>
      <text>1050-YSV-116</text>
      <text>1050-YSV-117</text>
      <text>1050-YSV-119</text>
      <text>1050-FICA-122</text>
      <text>Label 13</text>
      <text>Label 14</text>
      <text>Label 15</text>
    </labels>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_24</name>
    <pv_name>${StepDeviceName}:IntlckDevPerm1</pv_name>
    <x>1830</x>
    <y>211</y>
    <width>180</width>
    <height>29</height>
    <numBits>1</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="LED-YELLOW-ON" red="255" green="235" blue="17">
      </color>
    </on_color>
    <labels>
      <text>1070-YSV-006</text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
    </labels>
  </widget>
</display>
