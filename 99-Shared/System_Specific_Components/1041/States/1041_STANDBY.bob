<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-04-10 10:39:57 by EpingerBalint-->
<display version="2.0.0">
  <name>Stand By</name>
  <macros>
    <PLCName>Tgt-MRCS:Ctrl-PLC-001</PLCName>
  </macros>
  <width>2550</width>
  <height>1220</height>
  <background_color>
    <color name="BACKGROUND" red="220" green="225" blue="221">
    </color>
  </background_color>
  <grid_color>
    <color name="TEXT-LIGHT" red="230" green="230" blue="230">
    </color>
  </grid_color>
  <widget type="embedded" version="2.0.0">
    <name>X2</name>
    <macros>
      <StepName>STANDBY</StepName>
      <WIDDev>FSM</WIDDev>
      <WIDDis>SC</WIDDis>
      <WIDIndex>002</WIDIndex>
      <WIDSecSub>Tgt-PWCM1041</WIDSecSub>
      <Faceplate>../../System_Specific_Components/1041/States/1041_STANDBY.bob</Faceplate>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_BlockIcon_Standard_Compact.bob</file>
    <x>2040</x>
    <y>211</y>
    <width>500</width>
    <height>310</height>
    <resize>1</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>FlagLabel</name>
    <text>State Flag Status</text>
    <x>30</x>
    <y>680</y>
    <width>760</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BlkIconLabel</name>
    <text>State Block Icon</text>
    <x>2040</x>
    <y>170</y>
    <width>490</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ParameterLabel</name>
    <text>State Parameters</text>
    <x>30</x>
    <y>170</y>
    <width>760</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <macros>
      <Title>Primary Water Cooling Moderators - 1041 - Standby State</Title>
    </macros>
    <file>../../Header/WtrC_Header.bob</file>
    <width>2550</width>
    <height>85</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter1</name>
    <macros>
      <Description>Standby State Timer</Description>
      <EngUnit>min</EngUnit>
      <ParameterID>S1</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>210</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter2</name>
    <macros>
      <Description>1041-PICA-114/151 Main loop pressure control SP</Description>
      <EngUnit>barg</EngUnit>
      <ParameterID>S2</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>240</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter3</name>
    <macros>
      <Description>1041-FICA-150 Minimum pump flow control SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S3</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>270</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4</name>
    <macros>
      <Description>1041-HICA-124 Cooling water flow to Upper moderators SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S4</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>300</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_16</name>
    <pv_name>${StepDeviceName}:MeasPerm1</pv_name>
    <x>820</x>
    <y>211</y>
    <width>180</width>
    <height>440</height>
    <numBits>15</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </on_color>
    <labels>
      <text>PIT-103 OK</text>
      <text>LIA-104 OK</text>
      <text>LSZ-116 OK</text>
      <text>LSZ-117 OK</text>
      <text>QIT-112 OK</text>
      <text>PIT-114 OK</text>
      <text>FIT-113 OK</text>
      <text>PIT-118 OK</text>
      <text>TI-105 OK</text>
      <text>TI-109 OK</text>
      <text>TI-101 OK</text>
      <text>TI-102 OK</text>
      <text>TI-103 OK</text>
      <text>TI-104 OK</text>
      <text>W-001 OK</text>
      <text></text>
    </labels>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks</name>
    <text>State Specific Interlocks</text>
    <x>1430</x>
    <y>170</y>
    <width>580</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks_1</name>
    <text>State Entry Permissives</text>
    <x>820</x>
    <y>170</y>
    <width>580</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_19</name>
    <pv_name>${StepDeviceName}:MeasPerm2</pv_name>
    <x>1020</x>
    <y>211</y>
    <width>180</width>
    <height>440</height>
    <numBits>15</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </on_color>
    <labels>
      <text>FIT-124 OK</text>
      <text>FIT-125 OK</text>
      <text>FIT-129 OK</text>
      <text>FIT-130 OK</text>
      <text>PSZ-101 OK</text>
      <text>PSZ-102 OK</text>
      <text>PSZ-103 OK</text>
      <text>PSZ-104 OK</text>
      <text>1041 Pump(s) OK</text>
      <text>1070 Pump(s) OK</text>
      <text>1050-TI-153 OK</text>
      <text>1050-PI-152 OK</text>
      <text>1050-QI-150 OK</text>
      <text>1050-FI-122 OK</text>
      <text>1050-PS-151 OK</text>
      <text></text>
    </labels>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_1</name>
    <actions>
      <action type="open_display">
        <file>1041_OFF.bob</file>
        <macros>
          <StepDeviceName>Tgt-PWCM1041:SC-FSM-001</StepDeviceName>
        </macros>
        <target>replace</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>To Off</text>
    <x>10</x>
    <y>100</y>
    <width>160</width>
    <height>35</height>
    <tooltip>$(actions)</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <confirm_message>Open MAINTENANCE state view</confirm_message>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_2</name>
    <actions>
      <action type="open_display">
        <file>1041_RUNNING.bob</file>
        <macros>
          <StepDeviceName>Tgt-PWCM1041:SC-FSM-004</StepDeviceName>
        </macros>
        <target>replace</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>To Running</text>
    <x>2390</x>
    <y>100</y>
    <width>160</width>
    <height>35</height>
    <tooltip>$(actions)</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <confirm_message>Open MAINTENANCE state view</confirm_message>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_20</name>
    <pv_name>${StepDeviceName}:IntlckDevPerm1</pv_name>
    <x>1430</x>
    <y>211</y>
    <width>180</width>
    <height>177</height>
    <numBits>6</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="LED-YELLOW-ON" red="255" green="235" blue="17">
      </color>
    </on_color>
    <labels>
      <text>1041-PICA-114</text>
      <text>1041-PICA-151</text>
      <text>1041-HICA-102</text>
      <text>1041-TICA-109</text>
      <text>1041-FICA-150</text>
      <text>Fill &amp; Refill Substate</text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
    </labels>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_21</name>
    <pv_name>${StepDeviceName}:MeasPerm3</pv_name>
    <x>1220</x>
    <y>211</y>
    <width>180</width>
    <height>352</height>
    <numBits>12</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </on_color>
    <labels>
      <text>1070-FI-103 OK</text>
      <text>1070-FI-105 OK</text>
      <text>1070-TI-102 OK</text>
      <text>1070-TI-104 OK</text>
      <text>1070-PI-101 OK</text>
      <text>1050-FCV-122 OK</text>
      <text>1041-FCV-114 OK</text>
      <text>1041-FCV-124 OK</text>
      <text>1041-FCV-125 OK</text>
      <text>1041-FCV-129 OK</text>
      <text>1041-FCV-130 OK</text>
      <text>Purification ON</text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
    </labels>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4_1</name>
    <macros>
      <Description>1041-HICA-125 Cooling water flow to Upper moderators SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S5</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>330</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4_2</name>
    <macros>
      <Description>1041-HICA-129 Cooling water flow to Lower moderators SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S6</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>360</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4_3</name>
    <macros>
      <Description>1041-HICA-130 Cooling water flow to Lower moderators SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S7</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>390</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_23</name>
    <pv_name>${StepDeviceName}:IntlckDevPerm2</pv_name>
    <x>1630</x>
    <y>211</y>
    <width>180</width>
    <height>382</height>
    <numBits>13</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="LED-YELLOW-ON" red="255" green="235" blue="17">
      </color>
    </on_color>
    <labels>
      <text>1050-YSV-101</text>
      <text>1050-YSV-104</text>
      <text>1050-YSV-105</text>
      <text>1050-YSV-106</text>
      <text>1050-YSV-107</text>
      <text>1050-YSV-109</text>
      <text>1050-YSV-111</text>
      <text>1050-YSV-114</text>
      <text>1050-YSV-115</text>
      <text>1050-YSV-116</text>
      <text>1050-YSV-117</text>
      <text>1050-YSV-119</text>
      <text>1050-FICA-122</text>
      <text>Label 13</text>
      <text>Label 14</text>
      <text>Label 15</text>
    </labels>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>Byte Monitor_24</name>
    <pv_name>${StepDeviceName}:IntlckDevPerm1</pv_name>
    <x>1830</x>
    <y>211</y>
    <width>180</width>
    <height>29</height>
    <numBits>1</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </off_color>
    <on_color>
      <color name="LED-YELLOW-ON" red="255" green="235" blue="17">
      </color>
    </on_color>
    <labels>
      <text>1070-YSV-006</text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
    </labels>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter8</name>
    <macros>
      <Description>1050-FICA-122 Cooling water through purification SP</Description>
      <EngUnit>l/min</EngUnit>
      <ParameterID>S8</ParameterID>
    </macros>
    <file>../../../StateMachines/blockicons/StateMachine_parameter_Template.bob</file>
    <x>30</x>
    <y>420</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
</display>
