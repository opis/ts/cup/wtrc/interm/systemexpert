<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-09-12 13:50:06 by joricklochet-->
<display version="2.0.0">
  <name>ON_OFF_Control_Block Icon</name>
  <width>185</width>
  <height>105</height>
  <background_color>
    <color name="Transparent" red="255" green="255" blue="255" alpha="0">
    </color>
  </background_color>
  <grid_visible>false</grid_visible>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_1</name>
    <x>30</x>
    <y>35</y>
    <width>156</width>
    <height>66</height>
    <line_color>
      <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
      </color>
    </line_color>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <rules>
      <rule name="Visibility Rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>false</value>
        </exp>
        <pv_name>$(WIDESSControllerName):Regulation</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>WID_RegulationOFF_Background</name>
    <x>33</x>
    <y>50</y>
    <width>150</width>
    <height>36</height>
    <line_color>
      <color name="ATTENTION" red="252" green="242" blue="17">
      </color>
    </line_color>
    <background_color>
      <color name="ATTENTION" red="252" green="242" blue="17">
      </color>
    </background_color>
    <rules>
      <rule name="Visibility Rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>false</value>
        </exp>
        <pv_name>$(WIDESSControllerName):Regulation</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_RegulationMode</name>
    <text></text>
    <x>50</x>
    <y>50</y>
    <width>120</width>
    <height>36</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="29.0">
      </font>
    </font>
    <rules>
      <rule name="TextRule" prop_id="text" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>Reg. OFF</value>
        </exp>
        <pv_name>$(WIDESSControllerName):Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>Opmode indicator</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_Interlock</name>
    <symbols>
      <symbol>../../CommonSymbols/interlock_overridden_disabled_cms@32.png</symbol>
    </symbols>
    <y>74</y>
    <width>30</width>
    <height>30</height>
    <actions>
    </actions>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == false">
          <value>false</value>
        </exp>
        <pv_name>$(WIDESSControllerName):GroupInterlock</pv_name>
      </rule>
    </rules>
    <tooltip>Interlock event occured!</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_AlarmIcon</name>
    <symbols>
      <symbol>../../CommonSymbols/error@32.png</symbol>
    </symbols>
    <width>30</width>
    <height>30</height>
    <actions>
    </actions>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == false">
          <value>false</value>
        </exp>
        <pv_name>$(WIDESSControllerName):GroupAlarm</pv_name>
      </rule>
    </rules>
    <tooltip>Alarm event occured!</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_AUTMANIcon</name>
    <text>A</text>
    <y>22</y>
    <width>30</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="29.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="TextRule" prop_id="text" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value></value>
        </exp>
        <exp bool_exp="pv1 == true">
          <value>M</value>
        </exp>
        <pv_name>$(WIDESSControllerName):OpMode_Auto</pv_name>
        <pv_name>$(WIDESSControllerName):OpMode_Manual</pv_name>
      </rule>
    </rules>
    <tooltip>Opmode indicator</tooltip>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_Value_SP</name>
    <pv_name>$(WIDESSControllerName):FB_Setpoint</pv_name>
    <x>31</x>
    <y>69</y>
    <width>88</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="28.0">
      </font>
    </font>
    <background_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </background_color>
    <precision>1</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <rules>
      <rule name="Visibility Rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>$(WIDESSControllerName):Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>Setpoint</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_width>1</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_TitleLBL</name>
    <text>$(WIDControllerName)</text>
    <x>26</x>
    <width>153</width>
    <height>33</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="28.0">
      </font>
    </font>
    <background_color>
      <color red="0" green="0" blue="0" alpha="0">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <tooltip>Device name</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>WID_OpenFaceplate</name>
    <actions>
      <action type="open_display">
        <file>../faceplates/OnOff_Controller_PCV_Faceplate.bob</file>
        <macros>
          <ControllerName>$(WIDControllerName)</ControllerName>
          <ESSControllerName>$(WIDESSControllerName)</ESSControllerName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <width>185</width>
    <height>33</height>
    <background_color>
      <color red="236" green="236" blue="236" alpha="0">
      </color>
    </background_color>
    <transparent>true</transparent>
    <tooltip>Open faceplate</tooltip>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_Value_PV</name>
    <pv_name>$(WIDESSControllerName):ProcessValue</pv_name>
    <x>31</x>
    <y>36</y>
    <width>88</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="28.0">
      </font>
    </font>
    <background_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </background_color>
    <precision>1</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <rules>
      <rule name="Visibility Rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>$(WIDESSControllerName):Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>Measured Value</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_width>1</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_Value_SP_EGU_1</name>
    <pv_name>$(WIDESSControllerName):ProcValueEGU</pv_name>
    <x>119</x>
    <y>69</y>
    <width>66</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="24.0">
      </font>
    </font>
    <precision>1</precision>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <rules>
      <rule name="Disconnected" prop_id="background_color" out_exp="false">
        <exp bool_exp="pvInt0 &gt; 0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pvInt0 == 0">
          <value>
            <color name="INVALID" red="149" green="110" blue="221">
            </color>
          </value>
        </exp>
        <pv_name>${PLCName}:PLCHashCorrectR</pv_name>
      </rule>
      <rule name="Visible Rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>$(WIDESSControllerName):Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>Controller Setpoint</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_width>1</border_width>
    <border_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_Value_SP_EGU_2</name>
    <pv_name>$(WIDESSControllerName):ProcValueEGU</pv_name>
    <x>119</x>
    <y>36</y>
    <width>66</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="24.0">
      </font>
    </font>
    <precision>1</precision>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <rules>
      <rule name="Disconnected" prop_id="background_color" out_exp="false">
        <exp bool_exp="pvInt0 &gt; 0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pvInt0 == 0">
          <value>
            <color name="INVALID" red="149" green="110" blue="221">
            </color>
          </value>
        </exp>
        <pv_name>${PLCName}:PLCHashCorrectR</pv_name>
      </rule>
      <rule name="Visible Rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>$(WIDESSControllerName):Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>Controller Setpoint</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_width>1</border_width>
    <border_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </border_color>
  </widget>
</display>
