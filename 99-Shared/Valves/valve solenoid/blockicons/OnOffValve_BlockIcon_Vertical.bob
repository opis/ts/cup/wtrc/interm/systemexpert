<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-09-12 13:31:45 by joricklochet-->
<display version="2.0.0">
  <name>OnOffValve_BlockIcon_Vertical</name>
  <width>155</width>
  <height>120</height>
  <widget type="rectangle" version="2.0.0">
    <name>PLC_Disconnected</name>
    <y>31</y>
    <width>64</width>
    <height>60</height>
    <line_width>0</line_width>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <rules>
      <rule name="Disconnected background" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0&gt;0">
          <value>
            <color name="BACKGROUND" red="220" green="225" blue="221">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0==0">
          <value>
            <color name="INVALID" red="149" green="110" blue="221">
            </color>
          </value>
        </exp>
        <pv_name>$(PLCName):PLCHashCorrectR</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_CenterIconSolenoid</name>
    <pv_name>$(WIDESSDeviceName):SolenoidColor</pv_name>
    <symbols>
      <symbol>../symbols/solenoid_NOT_CONTROLED@64.png</symbol>
      <symbol>../symbols/solenoid_OFF@64.png</symbol>
      <symbol>../symbols/solenoid_ERROR@64.png</symbol>
      <symbol>../symbols/solenoid_NEUTRAL@64.png</symbol>
      <symbol>../symbols/solenoid_OK@64.png</symbol>
    </symbols>
    <y>29</y>
    <width>64</width>
    <height>64</height>
    <rotation>270.0</rotation>
    <actions execute_as_one="true">
    </actions>
    <tooltip>Open faceplate</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_CenterIcon</name>
    <pv_name>$(WIDESSDeviceName):ValveColor</pv_name>
    <symbols>
      <symbol>../symbols/valve_NOT_CONTROLED@64.png</symbol>
      <symbol>../symbols/valve_CLOSED@64.png</symbol>
      <symbol>../symbols/valve_ERROR@64.png</symbol>
      <symbol>../symbols/valve_NEUTRAL@64.png</symbol>
      <symbol>../symbols/valve_OK@64.png</symbol>
    </symbols>
    <y>29</y>
    <width>64</width>
    <height>64</height>
    <rotation>270.0</rotation>
    <actions execute_as_one="true">
    </actions>
    <tooltip>Open faceplate</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_AUTMANIcon</name>
    <text>E</text>
    <x>61</x>
    <y>39</y>
    <width>30</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="29.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="TextRule" prop_id="text" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value></value>
        </exp>
        <exp bool_exp="pv1 == true">
          <value>M</value>
        </exp>
        <exp bool_exp="pv2 == true">
          <value>F</value>
        </exp>
        <pv_name>$(WIDESSDeviceName):OpMode_Auto</pv_name>
        <pv_name>$(WIDESSDeviceName):OpMode_Manual</pv_name>
        <pv_name>$(WIDESSDeviceName):OpMode_Forced</pv_name>
      </rule>
    </rules>
    <tooltip>Opmode indicator</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_TitleLBL</name>
    <text>$(WIDDeviceName)</text>
    <x>53</x>
    <width>98</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="25.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
    <tooltip>Device name</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_Interlock</name>
    <symbols>
      <symbol>../../../CommonSymbols/interlock_overridden_disabled_cms@32.png</symbol>
    </symbols>
    <y>90</y>
    <width>30</width>
    <height>30</height>
    <actions>
    </actions>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == false">
          <value>false</value>
        </exp>
        <pv_name>$(WIDESSDeviceName):GroupInterlock</pv_name>
      </rule>
    </rules>
    <tooltip>Interlock event occured!</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_AlarmIcon</name>
    <symbols>
      <symbol>../../../CommonSymbols/error@32.png</symbol>
    </symbols>
    <width>30</width>
    <height>30</height>
    <actions>
    </actions>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == false">
          <value>false</value>
        </exp>
        <pv_name>$(WIDESSDeviceName):GroupAlarm</pv_name>
      </rule>
    </rules>
    <tooltip>Alarm event occured!</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>WID_OpenFaceplate</name>
    <actions>
      <action type="open_display">
        <file>../faceplates/OnOffValve_Faceplate.bob</file>
        <macros>
          <ESSDeviceName>$(WIDESSDeviceName)</ESSDeviceName>
          <DeviceName>$(WIDDeviceName)</DeviceName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <width>151</width>
    <height>118</height>
    <transparent>true</transparent>
    <tooltip>Open faceplate</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
</display>
