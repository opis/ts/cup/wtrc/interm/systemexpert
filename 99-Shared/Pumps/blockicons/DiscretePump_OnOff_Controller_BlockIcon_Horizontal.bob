<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-08-09 11:06:45 by joricklochet-->
<display version="2.0.0">
  <name>DiscretePump_OnOff_Controller_BlockIcon_Horizontal</name>
  <width>160</width>
  <height>210</height>
  <widget type="symbol" version="2.0.0">
    <name>WID_AlarmIcon</name>
    <symbols>
      <symbol>../../CommonSymbols/error@32.png</symbol>
    </symbols>
    <x>114</x>
    <y>33</y>
    <width>30</width>
    <height>30</height>
    <actions>
    </actions>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == false">
          <value>false</value>
        </exp>
        <pv_name>$(WIDESSDeviceName):GroupAlarm</pv_name>
      </rule>
    </rules>
    <tooltip>Alarm event occured!</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_Interlock</name>
    <symbols>
      <symbol>../../CommonSymbols/interlock_overridden_disabled_cms@32.png</symbol>
    </symbols>
    <x>19</x>
    <y>33</y>
    <width>30</width>
    <height>30</height>
    <actions>
    </actions>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == false">
          <value>false</value>
        </exp>
        <pv_name>$(WIDESSDeviceName):GroupInterlock</pv_name>
      </rule>
    </rules>
    <tooltip>Interlock event occured!</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_CenterIcon</name>
    <pv_name>$(WIDESSDeviceName):DeviceColor</pv_name>
    <symbols>
      <symbol>../symbols/P_ControlPump_NEUTRAL@64.png</symbol>
      <symbol>../symbols/P_ControlPump_ERROR@64.png</symbol>
      <symbol>../symbols/P_ControlPump_0RPM@64.png</symbol>
      <symbol>../symbols/P_ControlPump_OK@64.png</symbol>
    </symbols>
    <x>49</x>
    <y>44</y>
    <width>64</width>
    <height>64</height>
    <rotation>$(IconRotation)</rotation>
    <actions execute_as_one="true">
    </actions>
    <tooltip>Open faceplate</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_TitleLBL</name>
    <text>$(WIDDeviceName)</text>
    <x>10</x>
    <width>140</width>
    <height>35</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="28.0">
      </font>
    </font>
    <foreground_color>
      <color name="TEXT" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="BackGround Rule" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>
            <color name="BLUE" red="79" green="228" blue="250">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv1==1">
          <value>
            <color name="MAGENTA-BACKGROUND" red="218" green="195" blue="209">
            </color>
          </value>
        </exp>
        <pv_name>$(WIDESSDeviceName):OpMode_Auto</pv_name>
        <pv_name>$(WIDESSDeviceName):OpMode_Manual</pv_name>
      </rule>
    </rules>
    <tooltip>Device name</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>WID_OpenFaceplate</name>
    <actions>
      <action type="open_display">
        <file>../faceplates/DiscretePump_OnOffController_Faceplate.bob</file>
        <macros>
          <ESSDeviceName>$(WIDESSDeviceName)</ESSDeviceName>
          <ESSControllerName>$(WIDESSControllerName)</ESSControllerName>
          <DeviceName>$(WIDDeviceName)</DeviceName>
          <ControllerName>$(WIDControllerName)</ControllerName>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <width>160</width>
    <height>138</height>
    <transparent>true</transparent>
    <tooltip>Open faceplate</tooltip>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_Value_SP</name>
    <pv_name>$(WIDESSControllerName):FB_Setpoint</pv_name>
    <x>1</x>
    <y>138</y>
    <width>90</width>
    <height>33</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="28.0">
      </font>
    </font>
    <background_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </background_color>
    <precision>1</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <rules>
      <rule name="Visibility Rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>$(WIDESSControllerName):Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>Setpoint</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_width>1</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_Value_PV</name>
    <pv_name>$(WIDESSControllerName):ProcessValue</pv_name>
    <x>1</x>
    <y>172</y>
    <width>90</width>
    <height>33</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="28.0">
      </font>
    </font>
    <background_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </background_color>
    <precision>1</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <rules>
      <rule name="Visibility Rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>$(WIDESSControllerName):Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>Measured Value</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_width>1</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_Value_SP_EGU_1</name>
    <pv_name>$(WIDESSControllerName):ProcValueEGU</pv_name>
    <x>91</x>
    <y>138</y>
    <width>68</width>
    <height>33</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="28.0">
      </font>
    </font>
    <precision>1</precision>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <rules>
      <rule name="Disconnected" prop_id="background_color" out_exp="false">
        <exp bool_exp="pvInt0 &gt; 0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pvInt0 == 0">
          <value>
            <color name="INVALID" red="149" green="110" blue="221">
            </color>
          </value>
        </exp>
        <pv_name>$(PLCName):PLCHashCorrectR</pv_name>
      </rule>
      <rule name="Visible Rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>$(WIDESSControllerName):Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>Controller Setpoint</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_width>1</border_width>
    <border_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_Value_SP_EGU_2</name>
    <pv_name>$(WIDESSControllerName):ProcValueEGU</pv_name>
    <x>91</x>
    <y>172</y>
    <width>68</width>
    <height>33</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="28.0">
      </font>
    </font>
    <precision>1</precision>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <rules>
      <rule name="Disconnected" prop_id="background_color" out_exp="false">
        <exp bool_exp="pvInt0 &gt; 0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pvInt0 == 0">
          <value>
            <color name="INVALID" red="149" green="110" blue="221">
            </color>
          </value>
        </exp>
        <pv_name>$(PLCName):PLCHashCorrectR</pv_name>
      </rule>
      <rule name="Visible Rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>$(WIDESSControllerName):Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>Controller Setpoint</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_width>1</border_width>
    <border_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_AUTMANIcon_3</name>
    <text>PID:</text>
    <y>98</y>
    <width>58</width>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="TextRule" prop_id="text" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>PID:A</value>
        </exp>
        <exp bool_exp="pv1 == true">
          <value>PID:M</value>
        </exp>
        <pv_name>$(WIDESSControllerName):OpMode_Auto</pv_name>
        <pv_name>$(WIDESSControllerName):OpMode_Manual</pv_name>
      </rule>
    </rules>
    <tooltip>Opmode indicator</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_AUTMANIcon_4</name>
    <text>CD:</text>
    <y>80</y>
    <width>58</width>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <rules>
      <rule name="TextRule" prop_id="text" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>CD:A</value>
        </exp>
        <exp bool_exp="pv1 == true">
          <value>CD:M</value>
        </exp>
        <exp bool_exp="pv2 == true">
          <value>CD:F</value>
        </exp>
        <pv_name>$(WIDESSDeviceName):OpMode_Auto</pv_name>
        <pv_name>$(WIDESSDeviceName):OpMode_Manual</pv_name>
        <pv_name>$(WIDESSDeviceName):OpMode_Forced</pv_name>
      </rule>
    </rules>
    <tooltip>Opmode indicator</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_AUTMANIcon_5</name>
    <text>Reg:</text>
    <y>114</y>
    <width>58</width>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="TextRule" prop_id="text" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>Reg ON</value>
        </exp>
        <exp bool_exp="pv0 == false">
          <value>Reg OFF</value>
        </exp>
        <pv_name>$(WIDESSControllerName):Regulation</pv_name>
      </rule>
    </rules>
    <tooltip>Opmode indicator</tooltip>
  </widget>
</display>
