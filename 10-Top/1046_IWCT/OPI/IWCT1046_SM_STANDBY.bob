<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-11-06 13:44:05 by joricklochet-->
<display version="2.0.0">
  <name>TIWCS 1046 - STANDBY</name>
  <macros>
    <PLCName>Tgt-IWCT1046:Ctrl-PLC-001</PLCName>
  </macros>
  <width>2560</width>
  <height>1440</height>
  <background_color>
    <color name="BACKGROUND" red="220" green="225" blue="221">
    </color>
  </background_color>
  <grid_color>
    <color name="TEXT-LIGHT" red="230" green="230" blue="230">
    </color>
  </grid_color>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <file>IWCT1046_Header.bob</file>
    <width>2560</width>
    <height>90</height>
    <resize>2</resize>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>PermissivesWord3_1</name>
    <pv_name>${ESSDeviceName}:Permissive3</pv_name>
    <x>1226</x>
    <y>250</y>
    <height>780</height>
    <numBits>16</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="ERROR" red="252" green="13" blue="27">
      </color>
    </off_color>
    <labels>
      <text>PI - 328</text>
      <text>LI - 327</text>
      <text>TI - 329</text>
      <text>TI - 317</text>
      <text>FI - 318</text>
      <text>TI - 316</text>
      <text>PI - 312</text>
      <text>FI - 330</text>
      <text>TI - 320</text>
      <text>SICA-312</text>
      <text>TCV-316</text>
      <text></text>
      <text></text>
      <text>1010 System 
 permission</text>
      <text></text>
      <text>Current state 
    activated</text>
    </labels>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0==0">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0==1">
          <value>false</value>
        </exp>
        <pv_name>${ESSDeviceName}:STS_Active</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_1</name>
    <text>X2 - STANDBY</text>
    <y>90</y>
    <width>2557</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="BLACK" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <background_color>
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BlkIconLabel_2</name>
    <text>State status</text>
    <x>2021</x>
    <y>170</y>
    <width>490</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ParameterLabel_2</name>
    <text>State parameters</text>
    <x>41</x>
    <y>170</y>
    <width>760</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter1</name>
    <macros>
      <Description>Standby timer</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit>min</EngUnit>
      <PLCName>Tgt-IWCT1046:Ctrl-PLC-001</PLCName>
      <ParameterID>S1</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>210</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter2</name>
    <macros>
      <Description>PICA-128 Setpoint</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit>barg</EngUnit>
      <PLCName>Tgt-IWCT1046:Ctrl-PLC-001</PLCName>
      <ParameterID>S2</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>243</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter3</name>
    <macros>
      <Description>PICA-228 Setpoint</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit>barg</EngUnit>
      <PLCName>Tgt-IWCT1046:Ctrl-PLC-001</PLCName>
      <ParameterID>S3</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>276</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4</name>
    <macros>
      <Description>PICA-328 Setpoint</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit>barg</EngUnit>
      <PLCName>Tgt-IWCT1046:Ctrl-PLC-001</PLCName>
      <ParameterID>S4</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>309</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter5</name>
    <macros>
      <Description>TICA-116 Setpoint</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit>degC</EngUnit>
      <PLCName>Tgt-IWCT1046:Ctrl-PLC-001</PLCName>
      <ParameterID>S5</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>342</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter6</name>
    <macros>
      <Description>TICA-216 Setpoint</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit>degC</EngUnit>
      <PLCName>Tgt-IWCT1046:Ctrl-PLC-001</PLCName>
      <ParameterID>S6</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>375</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter7</name>
    <macros>
      <Description>TICA-316 Setpoint</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit>degC</EngUnit>
      <PLCName>Tgt-IWCT1046:Ctrl-PLC-001</PLCName>
      <ParameterID>S7</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>408</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter8</name>
    <macros>
      <Description>TICA-003 Setpoint</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit>degC</EngUnit>
      <PLCName>Tgt-IWCT1046:Ctrl-PLC-001</PLCName>
      <ParameterID>S8</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>441</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter9</name>
    <macros>
      <Description>TICA-004 Setpoint</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit>degC</EngUnit>
      <PLCName>Tgt-IWCT1046:Ctrl-PLC-001</PLCName>
      <ParameterID>S9</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>474</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter10</name>
    <macros>
      <Description>TICA-005 Setpoint</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit>degC</EngUnit>
      <PLCName>Tgt-IWCT1046:Ctrl-PLC-001</PLCName>
      <ParameterID>S10</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>509</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>PermissivesWord1_2</name>
    <pv_name>${ESSDeviceName}:Permissive1</pv_name>
    <x>851</x>
    <y>250</y>
    <height>780</height>
    <numBits>16</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="ERROR" red="252" green="13" blue="27">
      </color>
    </off_color>
    <labels>
      <text>PI - 128</text>
      <text>LI - 127</text>
      <text>TI - 129</text>
      <text>TI - 117</text>
      <text>FI - 118</text>
      <text>TI - 116</text>
      <text>PI - 112</text>
      <text>FI - 130</text>
      <text>TI - 120</text>
      <text>SICA-112</text>
      <text>TCV-116</text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
    </labels>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0==0">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0==1">
          <value>false</value>
        </exp>
        <pv_name>${ESSDeviceName}:STS_Active</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="byte_monitor" version="2.0.0">
    <name>PermissivesWord2_1</name>
    <pv_name>${ESSDeviceName}:Permissive2</pv_name>
    <x>1038</x>
    <y>250</y>
    <height>780</height>
    <numBits>16</numBits>
    <bitReverse>true</bitReverse>
    <horizontal>false</horizontal>
    <square>true</square>
    <off_color>
      <color name="ERROR" red="252" green="13" blue="27">
      </color>
    </off_color>
    <labels>
      <text>PI - 228</text>
      <text>LI - 227</text>
      <text>TI - 229</text>
      <text>TI - 217</text>
      <text>FI - 218</text>
      <text>TI - 216</text>
      <text>PI - 212</text>
      <text>FI - 230</text>
      <text>TI - 220</text>
      <text>SICA-212</text>
      <text>TCV-216</text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
      <text></text>
    </labels>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0==0">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0==1">
          <value>false</value>
        </exp>
        <pv_name>${ESSDeviceName}:STS_Active</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks_21</name>
    <text>State permissives</text>
    <x>851</x>
    <y>170</y>
    <width>534</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="Visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>false</value>
        </exp>
        <pv_name>${ESSDeviceName}:STS_Active</pv_name>
      </rule>
    </rules>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks_22</name>
    <text>Loop1</text>
    <x>851</x>
    <y>211</y>
    <width>160</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="Visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>false</value>
        </exp>
        <pv_name>${ESSDeviceName}:STS_Active</pv_name>
      </rule>
    </rules>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks_23</name>
    <text>Loop2</text>
    <x>1038</x>
    <y>211</y>
    <width>160</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="Visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>false</value>
        </exp>
        <pv_name>${ESSDeviceName}:STS_Active</pv_name>
      </rule>
    </rules>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks_24</name>
    <text>Loop3</text>
    <x>1226</x>
    <y>211</y>
    <width>160</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="Visibility" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>false</value>
        </exp>
        <pv_name>${ESSDeviceName}:STS_Active</pv_name>
      </rule>
    </rules>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>IWCT1046_SM_RUNNING.bob</file>
        <macros>
          <ESSDeviceName>Tgt-IWCT1046:SC-FSM-004</ESSDeviceName>
        </macros>
        <target>replace</target>
      </action>
    </actions>
    <text>To RUNNING</text>
    <x>2390</x>
    <y>127</y>
    <width>160</width>
    <height>35</height>
    <tooltip>$(actions)</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <confirm_message>Open RUNNING state view</confirm_message>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_1</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>IWCT1046_SM_OFF.bob</file>
        <macros>
          <ESSDeviceName>Tgt-IWCT1046:SC-FSM-001</ESSDeviceName>
        </macros>
        <target>replace</target>
      </action>
    </actions>
    <text>To OFF</text>
    <x>10</x>
    <y>127</y>
    <width>160</width>
    <height>35</height>
    <tooltip>$(actions)</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <confirm_message>Open OFF state view</confirm_message>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Interlocks</name>
    <x>1435</x>
    <y>170</y>
    <width>535</width>
    <height>469</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="label" version="2.0.0">
      <name>InterlockStatus_Title</name>
      <text>Interlocks status</text>
      <width>534</width>
      <height>32</height>
      <font>
        <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <transparent>false</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <border_width>1</border_width>
      <border_color>
        <color name="WHITE-BORDER" red="121" green="121" blue="121">
        </color>
      </border_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>InterlockStatus_Loop1_Subtitle</name>
      <text>Loop1</text>
      <y>40</y>
      <width>160</width>
      <height>32</height>
      <font>
        <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <transparent>false</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <border_width>1</border_width>
      <border_color>
        <color name="WHITE-BORDER" red="121" green="121" blue="121">
        </color>
      </border_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>InterlockStatus_Loop2_Subtitle</name>
      <text>Loop2</text>
      <x>187</x>
      <y>40</y>
      <width>160</width>
      <height>32</height>
      <font>
        <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <transparent>false</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <border_width>1</border_width>
      <border_color>
        <color name="WHITE-BORDER" red="121" green="121" blue="121">
        </color>
      </border_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>InterlockStatus_Loop3_Subtitle</name>
      <text>Loop3</text>
      <x>375</x>
      <y>40</y>
      <width>160</width>
      <height>32</height>
      <font>
        <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <transparent>false</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <border_width>1</border_width>
      <border_color>
        <color name="WHITE-BORDER" red="121" green="121" blue="121">
        </color>
      </border_color>
    </widget>
    <widget type="byte_monitor" version="2.0.0">
      <name>InterlockStatus_Loop1</name>
      <pv_name>${ESSDeviceName}:IntlckDevPerm1</pv_name>
      <y>79</y>
      <height>390</height>
      <bitReverse>true</bitReverse>
      <horizontal>false</horizontal>
      <square>true</square>
      <off_color>
        <color name="Background" red="220" green="225" blue="221">
        </color>
      </off_color>
      <on_color>
        <color name="Attention" red="252" green="242" blue="17">
        </color>
      </on_color>
      <labels>
        <text>       SICA - 112
(Stops if tank LL)</text>
        <text>            SICA - 112
(Stops if pressure HH)</text>
        <text>YSV - 114</text>
        <text>YSV - 130</text>
        <text>YSV - 121</text>
        <text>YSV - 122</text>
        <text>TICA - 116</text>
        <text>    SICA - 112
(Forced 100%)</text>
      </labels>
    </widget>
    <widget type="byte_monitor" version="2.0.0">
      <name>InterlockStatus_Loop2</name>
      <pv_name>${ESSDeviceName}:IntlckDevPerm2</pv_name>
      <x>187</x>
      <y>79</y>
      <height>390</height>
      <bitReverse>true</bitReverse>
      <horizontal>false</horizontal>
      <square>true</square>
      <off_color>
        <color name="Background" red="220" green="225" blue="221">
        </color>
      </off_color>
      <on_color>
        <color name="Attention" red="252" green="242" blue="17">
        </color>
      </on_color>
      <labels>
        <text>       SICA - 212
(Stops if tank LL)</text>
        <text>            SICA - 212
(Stops if pressure HH)</text>
        <text>YSV - 214</text>
        <text>YSV - 140</text>
        <text>YSV - 221</text>
        <text>YSV - 222</text>
        <text>TICA - 216</text>
        <text>    SICA - 212
(Forced 100%)</text>
      </labels>
    </widget>
    <widget type="byte_monitor" version="2.0.0">
      <name>InterlockStatus_Loop3</name>
      <pv_name>${ESSDeviceName}:IntlckDevPerm3</pv_name>
      <x>375</x>
      <y>79</y>
      <height>390</height>
      <bitReverse>true</bitReverse>
      <horizontal>false</horizontal>
      <square>true</square>
      <off_color>
        <color name="Background" red="220" green="225" blue="221">
        </color>
      </off_color>
      <on_color>
        <color name="Attention" red="252" green="242" blue="17">
        </color>
      </on_color>
      <labels>
        <text>       SICA - 312
(Stops if tank LL)</text>
        <text>            SICA - 312
(Stops if pressure HH)</text>
        <text>YSV - 314</text>
        <text>YSV - 150</text>
        <text>YSV - 321</text>
        <text>YSV - 322</text>
        <text>TICA - 316</text>
        <text>    SICA - 312
(Forced 100%)</text>
      </labels>
    </widget>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>X1_15</name>
    <macros>
      <Faceplate>../../../10-Top/1046_IWCT/OPI/IWCT1046_SM_STANDBY.bob</Faceplate>
      <WIDDeviceName>STANDBY</WIDDeviceName>
      <WIDESSDeviceName>Tgt-IWCT1046:SC-FSM-002</WIDESSDeviceName>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_BlockIcon_Standard.bob</file>
    <x>2021</x>
    <y>210</y>
    <width>500</width>
    <height>310</height>
    <resize>1</resize>
  </widget>
</display>
