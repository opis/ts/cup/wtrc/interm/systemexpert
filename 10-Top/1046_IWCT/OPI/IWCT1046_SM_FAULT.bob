<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-11-06 13:44:38 by joricklochet-->
<display version="2.0.0">
  <name>TIWCS 1046 - FAULT</name>
  <width>2557</width>
  <height>1285</height>
  <background_color>
    <color name="BACKGROUND" red="220" green="225" blue="221">
    </color>
  </background_color>
  <grid_color>
    <color name="TEXT-LIGHT" red="230" green="230" blue="230">
    </color>
  </grid_color>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <file>IWCT1046_Header.bob</file>
    <width>2560</width>
    <height>90</height>
    <resize>2</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <text>FAULT</text>
    <y>90</y>
    <width>2557</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="BLACK" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <background_color>
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter1</name>
    <macros>
      <Description>Minimum FAULT state time</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit>min</EngUnit>
      <ParameterID>S1</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>210</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter2</name>
    <macros>
      <Description>Spare</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit></EngUnit>
      <ParameterID>S2</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>243</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter3</name>
    <macros>
      <Description>Spare</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit></EngUnit>
      <ParameterID>S3</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>276</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter4</name>
    <macros>
      <Description>Spare</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit></EngUnit>
      <ParameterID>S4</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>309</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter5</name>
    <macros>
      <Description>TICA-116 Setpoint</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit>degC</EngUnit>
      <ParameterID>S5</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>342</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter6</name>
    <macros>
      <Description>TICA-216 Setpoint</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit>degC</EngUnit>
      <ParameterID>S6</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>375</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter7</name>
    <macros>
      <Description>TICA-316 Setpoint</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit>degC</EngUnit>
      <ParameterID>S7</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>408</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter8</name>
    <macros>
      <Description>TICA-003 Setpoint</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit>degC</EngUnit>
      <ParameterID>S8</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>441</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter9</name>
    <macros>
      <Description>TICA-004 Setpoint</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit>degC</EngUnit>
      <ParameterID>S9</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>474</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter10</name>
    <macros>
      <Description>TICA-005 Setpoint</Description>
      <EXAMPLE_MACRO>Value from Preferences</EXAMPLE_MACRO>
      <EngUnit>degC</EngUnit>
      <ParameterID>S10</ParameterID>
      <TEST>true</TEST>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Parameter_Template.bob</file>
    <x>40</x>
    <y>509</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BlkIconLabel_3</name>
    <text>State status</text>
    <x>2021</x>
    <y>170</y>
    <width>490</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ParameterLabel_3</name>
    <text>State parameters</text>
    <x>40</x>
    <y>170</y>
    <width>760</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks_17</name>
    <text>FAULT state entry conditions</text>
    <x>1140</x>
    <y>170</y>
    <width>534</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks_18</name>
    <text>Loop1</text>
    <x>1140</x>
    <y>212</y>
    <width>160</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks_19</name>
    <text>Loop2</text>
    <x>1327</x>
    <y>212</y>
    <width>160</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_StateInterlocks_20</name>
    <text>Loop3</text>
    <x>1515</x>
    <y>212</y>
    <width>160</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle</name>
    <x>1140</x>
    <y>254</y>
    <width>160</width>
    <height>50</height>
    <line_width>2</line_width>
    <line_color>
      <color name="BLACK-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </background_color>
    <rules>
      <rule name="Alarm" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>
            <color name="LED-RED-ON" red="255" green="60" blue="46">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv1==0">
          <value>
            <color name="DISCONNECTED" red="105" green="77" blue="164">
            </color>
          </value>
        </exp>
        <pv_name>Tgt-IWCT1046:Proc-LIA-127:ProcLoLo</pv_name>
        <pv_name>Tgt-IWCT1046:Ctrl-PLC-001:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_1</name>
    <text>LI-127 L2 alarm</text>
    <x>1140</x>
    <y>254</y>
    <width>160</width>
    <height>50</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_1</name>
    <x>1140</x>
    <y>314</y>
    <width>160</width>
    <height>50</height>
    <line_width>2</line_width>
    <line_color>
      <color name="BLACK-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </background_color>
    <rules>
      <rule name="Alarm" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>
            <color name="LED-RED-ON" red="255" green="60" blue="46">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv1==0">
          <value>
            <color name="DISCONNECTED" red="105" green="77" blue="164">
            </color>
          </value>
        </exp>
        <pv_name>Tgt-IWCT1046:Proc-PDT-130:ProcLoLo</pv_name>
        <pv_name>Tgt-IWCT1046:Ctrl-PLC-001:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_2</name>
    <text>FI-130 L2 alarm</text>
    <x>1140</x>
    <y>314</y>
    <width>160</width>
    <height>50</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_2</name>
    <x>1140</x>
    <y>374</y>
    <width>160</width>
    <height>50</height>
    <line_width>2</line_width>
    <line_color>
      <color name="BLACK-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </background_color>
    <rules>
      <rule name="Alarm" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>
            <color name="LED-RED-ON" red="255" green="60" blue="46">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv1==0">
          <value>
            <color name="DISCONNECTED" red="105" green="77" blue="164">
            </color>
          </value>
        </exp>
        <pv_name>Tgt-IWCT1046:Proc-PDT-118:ProcLoLo</pv_name>
        <pv_name>Tgt-IWCT1046:Ctrl-PLC-001:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_3</name>
    <text>FI-118 L2 alarm</text>
    <x>1140</x>
    <y>374</y>
    <width>160</width>
    <height>50</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_12</name>
    <x>1140</x>
    <y>494</y>
    <width>160</width>
    <height>50</height>
    <line_width>2</line_width>
    <line_color>
      <color name="BLACK-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </background_color>
    <rules>
      <rule name="Alarm" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>
            <color name="LED-RED-ON" red="255" green="60" blue="46">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv1==0">
          <value>
            <color name="DISCONNECTED" red="105" green="77" blue="164">
            </color>
          </value>
        </exp>
        <pv_name>Tgt-IWCT1046:Proc-P-009:ProcHiHi</pv_name>
        <pv_name>Tgt-IWCT1046:Ctrl-PLC-001:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_13</name>
    <text>P-009 H2 alarm</text>
    <x>1140</x>
    <y>494</y>
    <width>160</width>
    <height>50</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_13</name>
    <x>1140</x>
    <y>554</y>
    <width>160</width>
    <height>50</height>
    <line_width>2</line_width>
    <line_color>
      <color name="BLACK-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </background_color>
    <rules>
      <rule name="Alarm" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0==1 || pv1==1">
          <value>
            <color name="LED-RED-ON" red="255" green="60" blue="46">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv2==0">
          <value>
            <color name="DISCONNECTED" red="105" green="77" blue="164">
            </color>
          </value>
        </exp>
        <pv_name>Tgt-IWCT1046:Proc-SICA-112:BusControlOFF</pv_name>
        <pv_name>Tgt-IWCT1046:Proc-SICA-112:SStriggered</pv_name>
        <pv_name>Tgt-IWCT1046:Ctrl-PLC-001:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_14</name>
    <text>SICA-112 faulty</text>
    <x>1140</x>
    <y>554</y>
    <width>160</width>
    <height>50</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_14</name>
    <x>1327</x>
    <y>254</y>
    <width>160</width>
    <height>50</height>
    <line_width>2</line_width>
    <line_color>
      <color name="BLACK-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </background_color>
    <rules>
      <rule name="Alarm" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>
            <color name="LED-RED-ON" red="255" green="60" blue="46">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv1==0">
          <value>
            <color name="DISCONNECTED" red="105" green="77" blue="164">
            </color>
          </value>
        </exp>
        <pv_name>Tgt-IWCT1046:Proc-LIA-227:ProcLoLo</pv_name>
        <pv_name>Tgt-IWCT1046:Ctrl-PLC-001:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_15</name>
    <text>LI-227 L2 alarm</text>
    <x>1327</x>
    <y>254</y>
    <width>160</width>
    <height>50</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_15</name>
    <x>1327</x>
    <y>314</y>
    <width>160</width>
    <height>50</height>
    <line_width>2</line_width>
    <line_color>
      <color name="BLACK-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </background_color>
    <rules>
      <rule name="Alarm" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>
            <color name="LED-RED-ON" red="255" green="60" blue="46">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv1==0">
          <value>
            <color name="DISCONNECTED" red="105" green="77" blue="164">
            </color>
          </value>
        </exp>
        <pv_name>Tgt-IWCT1046:Proc-PDT-230:ProcLoLo</pv_name>
        <pv_name>Tgt-IWCT1046:Ctrl-PLC-001:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_16</name>
    <text>FI-230 L2 alarm</text>
    <x>1327</x>
    <y>314</y>
    <width>160</width>
    <height>50</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_16</name>
    <x>1327</x>
    <y>374</y>
    <width>160</width>
    <height>50</height>
    <line_width>2</line_width>
    <line_color>
      <color name="BLACK-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </background_color>
    <rules>
      <rule name="Alarm" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>
            <color name="LED-RED-ON" red="255" green="60" blue="46">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv1==0">
          <value>
            <color name="DISCONNECTED" red="105" green="77" blue="164">
            </color>
          </value>
        </exp>
        <pv_name>Tgt-IWCT1046:Proc-PDT-218:ProcLoLo</pv_name>
        <pv_name>Tgt-IWCT1046:Ctrl-PLC-001:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_17</name>
    <text>FI-218 L2 alarm</text>
    <x>1327</x>
    <y>374</y>
    <width>160</width>
    <height>50</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_17</name>
    <x>1327</x>
    <y>494</y>
    <width>160</width>
    <height>50</height>
    <line_width>2</line_width>
    <line_color>
      <color name="BLACK-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </background_color>
    <rules>
      <rule name="Alarm" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>
            <color name="LED-RED-ON" red="255" green="60" blue="46">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv1==0">
          <value>
            <color name="DISCONNECTED" red="105" green="77" blue="164">
            </color>
          </value>
        </exp>
        <pv_name>Tgt-IWCT1046:Proc-P-010:ProcHiHi</pv_name>
        <pv_name>Tgt-IWCT1046:Ctrl-PLC-001:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_18</name>
    <text>P-010 H2 alarm</text>
    <x>1327</x>
    <y>494</y>
    <width>160</width>
    <height>50</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_18</name>
    <x>1327</x>
    <y>554</y>
    <width>160</width>
    <height>50</height>
    <line_width>2</line_width>
    <line_color>
      <color name="BLACK-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </background_color>
    <rules>
      <rule name="Alarm" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0==1 || pv1==1">
          <value>
            <color name="LED-RED-ON" red="255" green="60" blue="46">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv2==0">
          <value>
            <color name="LED-GREEN-ON" red="70" green="255" blue="70">
            </color>
          </value>
        </exp>
        <pv_name>Tgt-IWCT1046:Proc-SICA-212:BusControlOFF</pv_name>
        <pv_name>Tgt-IWCT1046:Proc-SICA-212:SStriggered</pv_name>
        <pv_name>Tgt-IWCT1046:Ctrl-PLC-001:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_19</name>
    <text>SICA-212 faulty</text>
    <x>1327</x>
    <y>554</y>
    <width>160</width>
    <height>50</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_19</name>
    <x>1515</x>
    <y>254</y>
    <width>160</width>
    <height>50</height>
    <line_width>2</line_width>
    <line_color>
      <color name="BLACK-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </background_color>
    <rules>
      <rule name="Alarm" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>
            <color name="LED-RED-ON" red="255" green="60" blue="46">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv1==0">
          <value>
            <color name="DISCONNECTED" red="105" green="77" blue="164">
            </color>
          </value>
        </exp>
        <pv_name>Tgt-IWCT1046:Proc-LIA-327:ProcLoLo</pv_name>
        <pv_name>Tgt-IWCT1046:Ctrl-PLC-001:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_20</name>
    <text>LI-327 L2 alarm</text>
    <x>1515</x>
    <y>254</y>
    <width>160</width>
    <height>50</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_20</name>
    <x>1515</x>
    <y>314</y>
    <width>160</width>
    <height>50</height>
    <line_width>2</line_width>
    <line_color>
      <color name="BLACK-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </background_color>
    <rules>
      <rule name="Alarm" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>
            <color name="LED-RED-ON" red="255" green="60" blue="46">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv1==0">
          <value>
            <color name="DISCONNECTED" red="105" green="77" blue="164">
            </color>
          </value>
        </exp>
        <pv_name>Tgt-IWCT1046:Proc-PDT-330:ProcLoLo</pv_name>
        <pv_name>Tgt-IWCT1046:Ctrl-PLC-001:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_21</name>
    <text>FI-330 L2 alarm</text>
    <x>1515</x>
    <y>314</y>
    <width>160</width>
    <height>50</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_21</name>
    <x>1515</x>
    <y>374</y>
    <width>160</width>
    <height>50</height>
    <line_width>2</line_width>
    <line_color>
      <color name="BLACK-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </background_color>
    <rules>
      <rule name="Alarm" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>
            <color name="LED-RED-ON" red="255" green="60" blue="46">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv1==0">
          <value>
            <color name="DISCONNECTED" red="105" green="77" blue="164">
            </color>
          </value>
        </exp>
        <pv_name>Tgt-IWCT1046:Proc-PDT-318:ProcLoLo</pv_name>
        <pv_name>Tgt-IWCT1046:Ctrl-PLC-001:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_22</name>
    <text>FI-318 L2 alarm</text>
    <x>1515</x>
    <y>374</y>
    <width>160</width>
    <height>50</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_22</name>
    <x>1515</x>
    <y>434</y>
    <width>160</width>
    <height>50</height>
    <line_width>2</line_width>
    <line_color>
      <color name="BLACK-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </background_color>
    <rules>
      <rule name="Alarm" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>
            <color name="LED-RED-ON" red="255" green="60" blue="46">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv1==0">
          <value>
            <color name="DISCONNECTED" red="105" green="77" blue="164">
            </color>
          </value>
        </exp>
        <pv_name>Tgt-IWCT1046:Proc-P-011:ProcHiHi</pv_name>
        <pv_name>Tgt-IWCT1046:Ctrl-PLC-001:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_23</name>
    <text>P-011 H2 alarm</text>
    <x>1515</x>
    <y>434</y>
    <width>160</width>
    <height>50</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_23</name>
    <x>1515</x>
    <y>494</y>
    <width>160</width>
    <height>50</height>
    <line_width>2</line_width>
    <line_color>
      <color name="BLACK-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </background_color>
    <rules>
      <rule name="Alarm" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0==1 || pv1==1">
          <value>
            <color name="LED-RED-ON" red="255" green="60" blue="46">
            </color>
          </value>
        </exp>
        <exp bool_exp="pvInt2==0">
          <value>
            <color name="DISCONNECTED" red="105" green="77" blue="164">
            </color>
          </value>
        </exp>
        <pv_name>Tgt-IWCT1046:Proc-SICA-312:BusControlOFF</pv_name>
        <pv_name>Tgt-IWCT1046:Proc-SICA-312:SStriggered</pv_name>
        <pv_name>Tgt-IWCT1046:Ctrl-PLC-001:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_24</name>
    <text>SICA-312 faulty</text>
    <x>1515</x>
    <y>494</y>
    <width>160</width>
    <height>50</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>IWCT1046_SM_OFF.bob</file>
        <macros>
          <ESSDeviceName>Tgt-IWCT1046:SC-FSM-001</ESSDeviceName>
        </macros>
        <target>replace</target>
      </action>
    </actions>
    <text>To OFF</text>
    <x>2390</x>
    <y>127</y>
    <width>160</width>
    <height>35</height>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_24</name>
    <x>1327</x>
    <y>434</y>
    <width>160</width>
    <height>50</height>
    <line_width>2</line_width>
    <line_color>
      <color name="BLACK-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </background_color>
    <rules>
      <rule name="Alarm" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>
            <color name="LED-RED-ON" red="255" green="60" blue="46">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv1==0">
          <value>
            <color name="DISCONNECTED" red="105" green="77" blue="164">
            </color>
          </value>
        </exp>
        <pv_name>Tgt-IWCT1046:Proc-PIT-228:ProcLoLo</pv_name>
        <pv_name>Tgt-IWCT1046:Ctrl-PLC-001:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_25</name>
    <text>PI-228 L2 alarm</text>
    <x>1327</x>
    <y>434</y>
    <width>160</width>
    <height>50</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_25</name>
    <x>1140</x>
    <y>434</y>
    <width>160</width>
    <height>50</height>
    <line_width>2</line_width>
    <line_color>
      <color name="BLACK-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <background_color>
      <color name="LED-GREEN-ON" red="70" green="255" blue="70">
      </color>
    </background_color>
    <rules>
      <rule name="Alarm" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0==1">
          <value>
            <color name="LED-RED-ON" red="255" green="60" blue="46">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv1==0">
          <value>
            <color name="DISCONNECTED" red="105" green="77" blue="164">
            </color>
          </value>
        </exp>
        <pv_name>Tgt-IWCT1046:Proc-PIT-128:ProcLoLo</pv_name>
        <pv_name>Tgt-IWCT1046:Ctrl-PLC-001:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_26</name>
    <text>PI-128 L2 alarm</text>
    <x>1140</x>
    <y>434</y>
    <width>160</width>
    <height>50</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>X1_17</name>
    <macros>
      <Faceplate>../../../10-Top/1046_IWCT/OPI/IWCT1046_SM_FAULT.bob</Faceplate>
      <WIDDeviceName>FAULT</WIDDeviceName>
      <WIDESSDeviceName>Tgt-IWCT1046:SC-FSM-010</WIDESSDeviceName>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_BlockIcon_Standard.bob</file>
    <x>2021</x>
    <y>210</y>
    <width>500</width>
    <height>310</height>
    <resize>1</resize>
  </widget>
</display>
